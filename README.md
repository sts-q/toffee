# README #

# Toffee -- A programming language for Freeputer Virtual Machine ##

Toffee is a small programming language for Freeputer Virtual Machine (fvm).
It tries to be concise and light while all possibilities of Freeputer
are maintained.


See [www.Freeputer.net](http://www.freeputer.net/) or
[Freeputer-at-bitbucket](https://bitbucket.org/RobertGollagher/freeputer/overview).


-----------------------------------------------------------------------------


[TOC]


-----------------------------------------------------------------------------
# A Can of Toffees #





-----------------------------------------------------------------------------
# The Toffee programming language #


## Differences to Freelang ##

Toffee knows all instructions build into fvm, almost always under the same
name as known from Freelang. The following instructions have a different name
in Toffee:

	Freelang	Toffee
	&		&&
	|		||
	[&]		[&&]
	[|]		[||]



Toffee knows a 'call' instruction:  'call &function'.
Like in Freelang there is no 'lit &something'.
In Toffee it is impossible to jump into a function. 
Toffee puts functions as well as strings to the end of bytecode.


	for             Freelang                        Toffee
	strings         ." string "                     "string" or 'string'
	functions       : name ( comment ) ... ;        name{comment}...
	destination     go :dest                        go &dest

Porting from one to each other is a mainly a matter of syntacic differences,
but may lead to strange errors in some cases:

 * `go :dest` is compiled to `go: call dest` and not `go &dest`
 * copypaste `: rdrop ( -- ) rpop drop ;`
    adopt to `rdrop: rpop drop` and run into the following code (ret missing).


## Comments ##

	;This is a line comment.

## Keywords ##

        let   in   funs
        module interface end-of-module
        BEGIN   END 
        begin   end
        slotFloor
        slot
        type
        instr
        fn skip then see assert exit loop dip dip2 dip3 dip4 chr others

## Symbols ##

Characters in symbols may be one or more of: 
`!$%&*+-./0..9<=>?@A..Z[\]_a..z~` and `.

That is all from `!..~` without the following:

Not as first char: `.@&`

Not at all: `() {} "" '' :;, ^ # |`  (exception: '||' is fvm instr '|')

`(){} "" '' :, # |` are special characters, that separate symbols from each other:
`this#is(a|string)`  is parsed as `this  #  is  (  a  |  string  )`

`name >name name!  _name [name] ~name\a.s? 10%`  are vaild names.
`,` is a valid name and a special character. `.,` is parsed as `. ,`

	&name  compiles the address of name into bytecode.
	`name  puts the address of name on stack.
	@name  is sugar for `name @.
	!name  or name! is just a name. 
	name   calls function or instruction 'name'.

sourcecode  | description                       | bytecode      | datastack
----------- | -----------------------------     | -------------	|-----------
myname:	    | is a label                        | --            | --
myname{}    | is a function                     | --            | --
&10         | is value 10                       | 10            | --
&myname	    | is address of myname              | &myname       | --
10          | is number 10 on stack             | lit 10        | 10
`myname	    | is address of myname on stack     | lit &myname   | &myname
myname	    | is a call to label myname	        | call &myname  | --
`@myname`   | is content of addr/myn on stk     | lit &myname @ | cont


## Local Symbols ##

Basic mechanism for local names is let in funcs:

	; let foo and bar in bar and baz:
	let 
		foo {}
		bar {}
	in
		bar {}
		baz {}
	funcs

	; let g and h in f:	
	let
		g {}
		h {}
	in
		f {}
	funcs

	; let g and h in f, short version
	f {}
		.g {}
		.h {}

	; module deinitions are using let in funcs, too.
	module My
	foo {}
	fom {}
	interface 
	bar {}
	baz {}
	end-of-module My

	; is short for
	let 
		let 
			foo{}
			fom{}
		in
			bar{}
			bar{}
		funcs
	in
		My{}
	funcs

	; This module defintion defines:
	My{}
	My.bar{}
	My.baz{}
	My.bar.foo{}
	My.bar.fom{}



## Data types ##
	
* strings: `"This is a string."  'This is an other string.'`
  `"this 'too'" '"or" this'`
  Strings may go over multiple lines, the newlines go into the compiled string.
  NO Escape sequences so far: ( \n \t ... )

* numbers: decimal 32 bit integer: ... -2 1 0 1 2 ...



## Types ##

There is no support for a type system build into the language.

Antoffee just parses docstrings and looks, if the types used there are
defined. If not it prints an info message.

```
type date	i
type month	i

; Commenting the comment:
; Docstring format is {a b c -- d e f // some text}
fun {date -- monht  // get month out of date} second

; should give a message: #### type not defined: ~monht
```

Predefined are all letters: a..z


## Synonyms ##

In Toffee knows about the following synonyms:

	true        1
	false       0
	nil         0
	maxint	    2147483647
	minint	   -2147483648
	iftrue      go>=0
	iffalse     go==0
	if[true]    go[>0]
	if[false]   go[==0]
	ifnil       go[<=0]
	ifsome      go[>0]
	if[nil]     go[<=0]
	if[some]    go[>0]


## New Instructions ##

New words, that behave like fvm instructions, can be defined. 
The word definition is simply inlined. This is like inlining
functions or a simple macro. There are no local (in module)
new instructions possible.

	instr sdrop ( spop drop )
	instr rdrop ( rpop drop )
	instr pi    ( 355 113 )


## Rewriting ##

Toffee rewrites the following forms into Freelang style Freeputer instructions.

* (INSTR-LIST)dip		-> rpush INSTR-LIST rpop
* (INSTR-LIST)dip2		-> rpush rpush INSTR-LIST rpop rpop
* (INSTR-LIST)dip3		-> rpush rpush rpush INSTR-LIST rpop rpop rpop
* (INSTR-LIST)dip4		-> rpush rpush rpush rpush INSTR-LIST rpop rpop rpop rpop 
* go-cond skip(INSTR-LIST)	->          go-cond &thn   INSTR-LIST   thn:
* go-cond then(INSTR-LIST)	-> inverted-go-cond &thn   INSTR-LIST   thn:
* go-cond (INSTR-LIST)(INSTR-LIST)  -> go-cond &thn INST-LIST go &done thn: INST-LIST done:
* loop(INSTR-LIST)		-> loop: INSTR-LIST go &loop exit:
* fn(INSTR-LIST)     		-> `fn    ...        fn: INSTR-LIST ret
* chr'c'        		-> lit char-code-of-c
* (INSTR-LIST)see		-> INST-LIST string-of-instr-list
* (INSTR-LIST) int assert	-> fn(INSTR-LIST) int string-of-inst-list call &Test.i1

* see2(INSTR-LIST)              -> fn(INSTR-LIST) string-of-instr-list
* (VALUES)                      -> construct a list of VALUES

~~~
	name^     is syntactic sugar for   ( name )dip
	name^^    is syntactic sugar for   ( name )dip2
	name^^^   is syntactic sugar for   ( name )dip3
	name^^^^  is syntactic sugar for   ( name )dip4
	This ^-notation works for symbols, not for numbers: 
	name^^ spush^ true^ nil^

	; conditional expression (dsplit) is like scheme or lisps (cond...)
	| INSTR-LIST-1	| INSTR-LIST-2
	| INSTR-LIST-3	| INSTR-LIST-4
	others		| INSTR-LIST-n

	(cond
	  ((INSTR-LIST-1)   INSTR-LIST-2)
	  ((INSTR-LIST-3)   INSTR-LIST-4)
	  (else             INSTR-LIST-n))
~~~


What Antoffee exactly compiles can be seen best in ir.toffee, the intermediate 
representation of the compiled program.


-----------------------------------------------------------------------------
# Antoffee, the Toffee to Freeputer Compiler #

Antoffee reads Toffee source files, specifyed at command line (without
file extenstion `.toffee`) and writes four files:

 * `run/rom.fp`
   The known bytecode for fvm.

 * `ir/ir.toffee`
   An intermediate representation of the compiled program.

 * `doc/doc.txt`
   Info about the compiled program.

 * `asm/fp-x86-program.s`
   This is currently not working.

Antoffee is written in OCaml.
Antoffee knows no more command line parameters.

			
-----------------------------------------------------------------------------
# System Requirements #

Toffee is a programming language for Freeputer and therefore bytecode made
by Antoffee should run on any Freeputer instance.

Included in this repository is a fvm 1.0 16-16MB 256 stackmax (FVM HEAVY)
in source and binary form that is intended to run on 32-bit Linux.

Antoffee is written in OCaml.

A binary executable of Antoffee, compiled on 32-bit Linux, is included
in this repository. Antoffee is developed on OCaml Version 4.01 
(September 2013) without OPAM, core or any other dependencies and should 
compile on any Linux/Unix.

toffee-run is a bash script that calls antoffee and fvm with rom.fp


-----------------------------------------------------------------------------
# Install #

Download this repository and unpack. There is no further install process.

At command prompt say:

 * `./toffee-run`           Without parameters it prints further help.
 * `./toffee-run --hello`   It compiles and runs the hello.toffee program.

 * `./toffee-run -ca`       It removes the antoffee binary and tries
   to compile a new one. Consider to make a backup copy of old binary.

Of cause, antoffee can work on it's own, without any bash script.
Once it compiles a toffee source it always writes four files in four
different directories: ir/ir.toffee, run/rom.fp, doc/doc.txt and
asm/fp-x86-program.s

   
-----------------------------------------------------------------------------
# Freeputer #

Included in this repository is an instance of Freeputer. 
Freeputer is written by Robert Gollagher published  with 

	GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007

and under 
[Creative Commons Attribution-ShareAlike 4.0 International License.]
(https://creativecommons.org/licenses/by-sa/4.0/)

	(cc by-sa 4.0)

[www.Freeputer.net](http://www.freeputer.net/)

-----------------------------------------------------------------------------
# License #

    GNU GENERAL PUBLIC LICENSE
    Version 3, 29 June 2007

    Toffee   A programming language for Freeputer Virtual Machine (fvm).
    Copyright (C) 2016 Heiko Kuhrt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


-----------------------------------------------------------------------------
# Contact #

If you would like to contact the author please send message to

 Heiko.Kuhrt 

 yahoo.de 

Toffee language was first published at 6. December 2016.

-----------------------------------------------------------------------------
# ToDo #

 * '[  ||  ]' is parsed as '[||]', that is toffee's for fvm instr '[|]'.
 * `,` as function name can not be a local dot-function-name: `.,{}...`
 * Improve error messages.
 * Local labels within functions.
 * Maximal source file size is currently 64kB.
 * ir.toffee is not valid toffee source for '("string")see'.
 * Support for gnu-as is currently not working.


-----------------------------------------------------------------------------

