;;=============================================================================
;;
;; toffee-mode.el
;; emacs syntax highlighting for toffee
;;
;;=============================================================================
;;
;; README
;;
;;
;;
;; CONTACT
;;
;; https:/bitbucket.org/sts-q
;;
;;
;; LICENCE
;;
;; GPL 3.0
;;
;;=============================================================================

;;     -*- mode: toffee -*-
;;

(add-to-list 'auto-mode-alist '("\\.toffee\\'" . toffee-mode))

;;=============================================================================

;; let <tab> key insert 4 spaces as freelang does not like tabs.
;(add-hook 'toffee-mode-hook
;	  (lambda ()
;	    (define-key toffee-mode-map (kbd "<tab>")
;	      (lambda() (interactive) (insert "    ")))))

(defun toffee-indent-line-fun()
  "Indent current line for toffee files."
  (interactive)
  (insert "[toffee-indent-line-fun]")
  nil)

(add-hook 'toffee-mode-hook
	  (lambda ()
	    (define-key toffee-mode-map (kbd "<return>")   'align-newline-and-indent)
	    (define-key toffee-mode-map (kbd "M-i")        'toffee-indent-line-fun)
	    (define-key toffee-mode-map (kbd "<tab>")      (lambda() (interactive) (insert "\t")))
	    ))


;;=============================================================================


(defun toffee-font-lock-fun()
  "return a ( regexp . color ) list"
  (let* ((head   "\\<")                
	 (tail   "\\>")
	 (word   "[[:word:]]+")
	 (stack-comment   "( [^)]* )")
	 (faces 
	  `( 
	    
	    ;; ---------------------------------------------------------
	    ;; function definition
	     (,(concat "\\(" head word tail "\\)\\s-*\\({[^}]*}\\)")         ; {[^}]*}
	      (1  font-lock-function-name-face)
	      (2  font-lock-doc-face))

	    ;; label
	    (,(concat word "\\:")  .  font-lock-variable-name-face)
	    
	    
	    ;; compile address into bytecode: :label
	    (,(concat  "&" word)  .  font-lock-builtin-face)
	    
	    ;; put address on stack: lit :label
	    (,(concat "`" word)  .  font-lock-constant-face)
	    
	    ;; ---------------------------------------------------------
	    
	    ;; warning
	    (,(concat head (regexp-opt
			    '("tron" "troff"
			      "xx"
			      ) t) tail)  .  font-lock-warning-face)
	    
	    ;; jump condition
	    (,(concat head (regexp-opt
			    '(
			      "go"
			      "go>0"   "go>=0"   "go==0"   "go!=0"   "go<=0"   "go<0"
			      "go[>0]" "go[>=0]" "go[==0]" "go[!=0]" "go[<=0]" "go[<0]"
			      
			      "go>"   "go>="   "go=="   "go!="   "go<="   "go<"
			      "go[>]" "go[>=]" "go[==]" "go[!=]" "go[<=]" "go[<]"

			      ;;;---------------------------------------------
			      ;;; not part of freeputer instruction set
                              ;;; delete or outcomment if not wanted
			      "ifsome"    "ifnil"    "iffail"    "ifnone"    "ifany"    "ifok"
			      "if[some]"  "if[nil]"  "if[fail]"  "if[none]"  "if[any]"  "if[ok]"
			      "iftrue" "iffalse"
			      "if[true]" "if[false]"
			      "return" "drop_return" "drop2_return" "spop_drop2_return"
			      "exit" "then" "skip" "else" "fn" "#" "others" "begin" "end"
			      ;;;---------------------------------------------
			      ) t) tail)  .  font-lock-builtin-face)
	    
	    ;; fvm instruction set
	    (,(concat head (regexp-opt
			    '(
			      "==="
			      "reador" "writor" "tracor" "getor" "putor"
			      "readorb" "writorb" "tracorb" "getorb" "putorb" 
			      
			      "ret" "invoke" "[invoke]" "fly"
			      
			      "swap" "over" "rot" "tor" "leap" "nip" "tuck"

			      
			      ;; fvm 1.1
			       "rot4" "tor4" "rev4" "swap2" "zoom"
			       "[fly]" "pc?"
			       "math" "trap" "die"
			       "pchan?""gchan?""wchan?""rchan?"
			       "read?" "write?" "get?" "put?"
			       "trace?"

			       			      
			      "rev"
			      "rpush" "rpop"
			      "drop" "drop2" "drop3" "drop4"
			      "dup" "dup2" "dup3" "dup4"
			      "hold" "hold2" "hold3" "hold4"
			      "speek" "speek2" "speek3" "speek4"
			      "spush" "spush2" "spush3" "spush4"
			      "spop" "spop2" "spop3" "spop4"
			      
			      "dec" "decw" "dec2w" "inc" "incw" "inc2w"
			      
			      "@" "!" "[@]" "@b" "!b" "[@b]"
			      "@@" "@!" "[@@]" "@@b" "@!b" "[@@b]"
			      
			      "+" "-" "*" "/" "%" "/%"
			      "[+]" "[-]" "[*]" "[/]" "[%]" "[/%]"
			      "neg" "abs"
			      "&" "|" "^" "[&]" "[|]" "[^]"
			      "<<" ">>" "[<<]" "[>>]"
			      
			      "move" "fill" "find" "match"
			      "moveb" "fillb" "findb" "matchb"
			      
			      "homio"
			      "rchan" "wchan" "gchan" "pchan"
			      
			      "ecode?" "rcode?" "rom?" "ram?" "map?" "stdblk?"
			      "ds?" "ss?" "rs?" "dsn?" "ssn?" "rsn?"
					;"tron" "troff"  ; we have these at warings
			      "reset" "reboot" "halt" "data"
			      ) t) tail)  .  font-lock-type-face)
	    
	    ;; toffee keywords
	    (,(concat head (regexp-opt '("BEGIN" "END" "slot" "type" "instr" "deinstr"
					 "slotFloor"
					  "let" "in" "loc" "with" "where" "funcs" "functions"
					  "module" "interface" "implementation" "end-of-module"
					  "loop" "loop^" "loop^^"  "recur" "rec"
					  "TX") t) tail)		.  font-lock-keyword-face)
	    ;; ---------------------------------------------------------
	    ;; numbers
	    (,(concat head (regexp-opt
			    '("some" "nil" "fail" "true" "false"
			      ) t) tail)  .  font-lock-constant-face)
	    
	    (,(concat head "-[0-9\\.]+" tail)    .  font-lock-builtin-face)
	    (,(concat head "[\\+]?[0-9]+" tail)  .  font-lock-constant-face)
	    ;; ---------------------------------------------------------
	    
	    )))
    faces))

;;=============================================================================

(define-derived-mode   toffee-mode prog-mode "toffee" 
  "Define major mode for editing toffee files."
  (setf font-lock-defaults       (list (toffee-font-lock-fun)))
)

(mapcar (lambda (p)
	  (modify-syntax-entry (car p) (cadr p) toffee-mode-syntax-table))
	'((?:   "W")     ; take :_?...!@ as word chars
	  (?_   "W")
	  (??   "W")
	  (?+   "W")
	  (?-   "W")
	  (?*   "W")
	  (?/   "W")
	  (?<   "W")
	  (?>   "W")
	  (?=   "W")
	  (?.   "W")
	  (?[   "W")
	    (?]   "W")
	  (?#   "W")
	  (?$   "W")
	  (?!   "W")
	  (?@   "W")
	  (?~   "W")
	  (?\&   "W")
	  (?\\   "W")
	  (?`   "W")
	  
	  (?'    "\"")    ; "'" is string delimiter: highlight 'x' like string "x"

  	  (?;  "< b")      ; ; starts comment
	  (?\n "> b")      ; ; comment up to end of line

	  
	  ))

(provide 'toffee-mode)
;;=============================================================================

