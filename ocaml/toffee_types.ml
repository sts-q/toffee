(**  rabbit_types.ml *)

open List
open Lib


type  item =	| Sym	of string
		| Char 		of char
		| Str  		of string
		| Docstr 	of string
		| Int 		of int32
		| Bool 		of bool

		| Lst 		of item list

		| Label		of string
		| Dest		of string
		| Word		of int32

		| Line 		of int
		| Typ		of string
		| Space		of int

let zero = Int32.zero
let one  = Int32.one
let two  = Int32.of_int 2

(*==========================================================================*)
(*   :section:   show   *)
(*--------------------------------------------------------------------------*)

let show_item' str_start str_end sep  =
	
	let rec loop =
		function
		| Sym s 	->  s 			^ sep
		| Char c	->  Char.escaped c	^ sep
		| Str s		->  str_start ^ s ^ str_end ^ (if String.length s > 0 && s.[String.length s - 1] <> '\n' then sep else "")
		| Docstr d	->  "{" ^ d ^ "}"
		| Int i		->  Int32.to_string i 	^ sep
		| Bool b	->  if   b   then   "true"^sep   else   "false"^sep

		| Lst l		->  "("     ^ (String.concat "" (map loop l))^")"

		| Label	l	->  l ^ ":"		^ sep
		| Dest d	->  "&" ^ d		^ sep
		| Word w 	->  Int32.to_string w	^ sep

		| Line l 	->  "<" ^ (string_of_int l) ^ ">" ^sep
		| Typ t		->  "~" ^ t ^ ""
		| Space i  	->  "" ^ string_of_int i ^ "... "
	in loop 


let show_item     = show_item' "\"" "\"" " "

let print_item text item =
	ps text; lf();
	ps (show_item item);lf();
	lf()

(*==========================================================================*)

let currentline = ref 0
let currentfile = ref ""

let warn s t line = print_string ( "####  <" ^ string_of_int line ^">  "^ s ^ ": " ^ show_item t ^ nl )

let section name file_type =
  "; ============================================================================" ^ nl
^ ";   :section:   " ^ name ^ nl
^ "; ----------------------------------------------------------------------------" ^ nl
	

let gensym () = "%_" ^ nextGenname()

let branch_symbols = [
	"go>0";   "go>=0";   "go==0";   "go!=0";   "go<=0";   "go<0";
	"go[>0]"; "go[>=0]"; "go[==0]"; "go[!=0]"; "go[<=0]"; "go[<0]";
			      
	"go>";   "go>=";   "go==";   "go!=";   "go<=";   "go<";
	"go[>]"; "go[>=]"; "go[==]"; "go[!=]"; "go[<=]"; "go[<]";
	]

let invert_gocond = function
	| "go>0"     ->  "go<=0"
	| "go>=0"    ->  "go<0"
	| "go==0"    ->  "go!=0"
	| "go!=0"    ->  "go==0"
	| "go<=0"    ->  "go>0"
	| "go<0"     ->  "go>=0"
	| "go[>0]"   ->  "go[<0]"
	| "go[>=0]"  ->  "go[<=0]"
	| "go[==0]"  ->  "go[!=0]"
	| "go[!=0]"  ->  "go[==0]"
	| "go[<=0]"  ->  "go[>0]"
	| "go[<0]"   ->  "go[>=0]"
	| "go>"      ->  "go<="
	| "go>="     ->  "go<"
	| "go=="     ->  "go!="
	| "go!="     ->  "go=="
	| "go<="     ->  "go>"
	| "go<"      ->  "go>="
	| "go[>]"    ->  "go[<=]"
	| "go[>=]"   ->  "go[>]"
	| "go[==]"   ->  "go[!=]"
	| "go[!=]"   ->  "go[==]"
	| "go[<=]"   ->  "go[>]"
	| "go[<]"    ->  "go[>=]"
	| s  ->  failwith ("invert_gocond: no gocond found: " ^ s)

let words_of_string s =
	let l = String.length s in
	let rec loop = function
		| i when i < l  ->  Word (Int32.of_int (Char.code (s.[i]))) :: loop (i+1)
		| others        ->  []
	in
	Word (Int32.of_int l) :: loop 0

let unlst (Lst ts) = ts

let without_lines ts =
	let rec loop = function
	| []  ->  []
	| Line _ :: ts  ->  loop ts
	| t :: ts  ->  t :: loop ts
	in
	loop ts

(*==========================================================================*)
let emit (Lst ts) =
	let bytes_of_word w =
		let open Int32 in
		let a_byte_mask =  of_int( 255 lsl 24 ) in
		let b_byte_mask =  of_int( 255 lsl 16 ) in
		let c_byte_mask =  of_int( 255 lsl  8 ) in
		let d_byte_mask =  of_int(       255  ) in
(*		(to_int  ((w logand a_byte_mask) shift_right 24 logand 255),
		 to_int  ((w logand b_byte_mask) shift_right 16),
		 to_int  ((w logand c_byte_mask) shift_right  8),
		 to_int   (w logand d_byte_mask)
		)  *)
		(to_int  (shift_right_logical   (logand w a_byte_mask)  24),
		 to_int  (shift_right           (logand w b_byte_mask)  16),
		 to_int  (shift_right           (logand w c_byte_mask)   8),
		 to_int                         (logand w d_byte_mask)
		)
	in
	let len = 4 * (length ts) in
	let outs = String.make len (Char.chr 0) in
	let insert pos word = 
		let (a,b,c,d) = bytes_of_word word
			in begin
			outs.[pos    ] <- Char.chr d;
			outs.[pos + 1] <- Char.chr c;
			outs.[pos + 2] <- Char.chr b;
			outs.[pos + 3] <- Char.chr a;
			end 
	in
	let rec loop pc = function
		| []  ->  outs
		| Word word :: ts  -> ( insert pc word;   loop (pc+4) ts   )
		| t :: ts  ->  failwith ("emit: only words, not: " ^ (show_item t))
	in
	loop 0 ts

(*==========================================================================*)
(*   :section:   parse   *)
(*--------------------------------------------------------------------------*)

let desugar_word word  =
	let (prechar,nword) =
		if    String.length word > 1
		   &&     String.contains  "`&@" word.[0]
	    (*	   && not(String.contains  "<>="  word.[1])   *)       (* 29.11.2016  del *)
			then (	word.[0],
				(String.sub word 1 ((String.length word) - 1)))
			else (' ', word)
	in
	let d_pos       = ref 0  in
	let nword_len   = String.length nword in
	let find_d_pos ()  =
		while !d_pos < nword_len && nword.[!d_pos] <> '^' do
			incr d_pos
			done
	in
	find_d_pos();
	let rec has_only_dips  = function
		| i when i < nword_len && nword.[i] = '^'  ->  has_only_dips (i+1)
		| i when i < nword_len  ->  false
		| i                     ->  true
	in
	let (w,d)  =  if !d_pos = nword_len
			then  (nword,0)
			else  ( if not (has_only_dips (!d_pos + 1))
				then failwith ("desugar_word: '^' only at word end: " ^ nword );
				(String.sub nword 0 !d_pos,   nword_len - !d_pos);)
	in
	match  word  with
	| "`"	->  failwith "No single '`' as word."
   (*	| "&"	->  failwith "No single '&' as word. Use '&&' for bit-or-operations."  *)  (* &"string it is" *)
	| ""	->  failwith ("desugar_word: empty word: '" ^ word ^ "'")
	| word  ->  (prechar,w,d)

let wrap_dips (p:item list) = function
	| 0  ->  p
	| 1  ->  [Lst p; Sym "dip"]
	| 2  ->  [Lst p; Sym "dip2"]
	| 3  ->  [Lst p; Sym "dip3"]
	| 4  ->  [Lst p; Sym "dip4"]
	| _  ->  failwith "wrap_dips: at max 4 dips"

let addr_operator (word:item) =
	let dest_or_word s =
		try Word (Int64.to_int32 (int64_of_signed_string s)) with
		| Failure _ -> Dest s
	in
	match word with
	| Sym s   ->  dest_or_word s
	| others  ->  failwith ("addr_operator: strange item: " ^ show_item word )

let wrap_sugar (word:item)  = function
	| ' '	->  [word]
	| '`'	->  [Sym "`"; word]
	| '&'	->  [addr_operator word]
	| '@'	->  [Sym "`";  word; Sym "@"]
	| c	->  failwith ("wrap_sugar: strange char found (only: `&@):" ^ Char.escaped c)

let translate_synonym  = function			(* not in rewrite wg pattern matching *)
	| "true"      ->  Int Int32.one
	| "false"     ->  Int Int32.zero
	| "maxint"    ->  Int (Int32.of_string  "2147483647")
	| "minint"    ->  Int (Int32.of_string "-2147483648")
	| "ifnone"    ->  Sym "go<=0"
	| "if[none]"  ->  Sym "go[<=0]"
	| "iftrue"    ->  Sym "go>0"
	| "iffalse"   ->  Sym "go==0"
	| "if[true]"  ->  Sym "go[>0]"
	| "if[false]" ->  Sym "go[==0]"

	| "nil"       ->  Int Int32.zero
	| "ifnil"     ->  Sym "go<=0"
	| "ifsome"    ->  Sym "go>0"
	| "if[nil]"   ->  Sym "go[<=0]"
	| "if[some]"  ->  Sym "go[>0]"

	|  w          ->  Sym w

let parse_word (w:string)  =
	let ((prechar:char),(word:string),(dips:int))  =  desugar_word w
	in 
	wrap_dips (wrap_sugar (translate_synonym word) prechar) dips

(*--------------------------------------------------------------------------*)

let parse src =
	let rec loop  =  function
	| []	->  []
	| Parse.Word "[" :: Parse.Word "||" :: Parse.Word "]" :: ts
	                                        ->      Sym "[||]" :: loop ts
	| t::ts ->  begin
			match  t  with
			| Parse.Newline (l,i)	->	Line l :: loop ts
			| Parse.Comment _	->	loop ts
			| Parse.KeyChar ":"	-> 	Sym ":"  :: (loop ts)
			| Parse.KeyChar "|"	->	Sym "|"  :: (loop ts)
			| Parse.KeyChar "#"	->	Sym "#"  :: (loop ts)
			| Parse.KeyChar ","	->	Sym ","  :: (loop ts)
			| Parse.Word "&&"	->	Sym "&&" :: loop ts
			| Parse.Word "||"	->	Sym "||" :: loop ts
			| Parse.Word "^"	->	Sym "^"  :: loop ts
			| Parse.Word "[^]"	->	Sym "[^]" :: loop ts
			| Parse.Word "@@"	->	Sym "@@" :: loop ts
			| Parse.Word "@^"	->	[Lst [Sym "@";]; Sym "dip";] @ loop ts
			| Parse.Word w		->	parse_word w    @ (loop ts)  
			| Parse.Int  i		->	(*  ( if abs i > 2147483647 then failwith ("integer literal too big: " ^ string_of_int i);  *)
							Int i    :: (loop ts) 
			| Parse.Char _		->	failwith "parse: can't parse char"
			| Parse.String1	s	->	Str s    :: (loop ts)
			| Parse.String2 s	->	Str s	 :: (loop ts)
			| Parse.String3 s	->	Docstr s :: (loop ts)
			| Parse.ParenList mts	->	Lst (loop mts)  :: (loop ts)
			| Parse.BraketList mts  -> 	Lst (loop mts)  :: (loop ts)
			| Parse.BeginList  mts  ->	Lst (loop mts)  :: (loop ts)
			| Parse.BEGINList  mts  ->	Lst (loop mts)  :: (loop ts)

			| t			->	failwith ("parse: stange pattern found: " ^ Parse.show_token t)
		    end
	in
	match  Parse.toffee src  with
	| Parse.SimplyList ts	->  Lst (loop ts)
	| _			->  failwith "Rabbit_run.parse: got no SimplyList from Parse.rabbit"


(*==========================================================================*)
(*    :section:   exceptions   *)
(*--------------------------------------------------------------------------*)
exception Compilation_failed
exception Error

let ccs ts text =
	ps "\n###################################"; lf();
	ps text;  lf();
	if !currentfile <> "" then (ps "file: "; ps !currentfile;  	lf(););
	if !currentline <>  0 then (ps "line: "; pi !currentline;       lf(););   
	if ts           <> [] then (ps (show_item (Lst (take ts 10)));	lf(););
	lf();
	ps "Compilation_failed"; lf();
	raise Compilation_failed

let error text =
	ps "\n###################################\n";
	ps text;
	lf();
	ps "Error";
	lf();
	raise Error

(*==========================================================================*)
(*   :section:   docstring   *)
(*--------------------------------------------------------------------------*)
let parse_docstring s =
	let rec loop = function
		| []  ->  []
		| Parse.Word w :: ts  ->  Typ w :: loop ts
		| Parse.ParenList l :: ts  ->  Lst( loop l ) :: loop ts
		| x :: ts  ->  failwith ("parse_docstring: what?\n{" ^ s ^"}")
	in
	let rec split intyp = function
		| t :: ts when t = Typ "--"  ->  (rev intyp, ts)
		| t :: ts  ->  split (t :: intyp) ts
		| []  ->  failwith ("#######  parse_docstring: '--' missing:\n{" ^ s ^"}")
	in

	match Parse.docstring s with
	| Parse.SimplyList [ Parse.SimplyList typ; Parse.String3 text ]
		->	let (intyp, outtyp) = if length typ > 0
						then split [] (loop typ)
						else ([],[])
			in
		Lst [Lst intyp; Lst outtyp; Str text]

	| _  ->  failwith "parse_docstring: pattern match failed"

(*==========================================================================*)
(* :section:   types   *)
(*--------------------------------------------------------------------------*)
let types_table = Hashtbl.create 100

let add_typ k v = Hashtbl.add types_table k v
let has_typ k   = Hashtbl.mem types_table k
let show_typ k v = "\t" ^ show_item k ^ "\t\t " ^ show_item v ^ "\n"


;; add_typ (Typ "a") (Lst [])
;; add_typ (Typ "b") (Lst [])
;; add_typ (Typ "c") (Lst [])
;; add_typ (Typ "d") (Lst [])
;; add_typ (Typ "e") (Lst [])
;; add_typ (Typ "f") (Lst [])
;; add_typ (Typ "g") (Lst [])
;; add_typ (Typ "h") (Lst [])
;; add_typ (Typ "i") (Lst [])
;; add_typ (Typ "j") (Lst [])
;; add_typ (Typ "k") (Lst [])
;; add_typ (Typ "l") (Lst [])
;; add_typ (Typ "m") (Lst [])
;; add_typ (Typ "n") (Lst [])
;; add_typ (Typ "o") (Lst [])
;; add_typ (Typ "p") (Lst [])
;; add_typ (Typ "q") (Lst [])
;; add_typ (Typ "r") (Lst [])
;; add_typ (Typ "s") (Lst [])
;; add_typ (Typ "t") (Lst [])
;; add_typ (Typ "u") (Lst [])
;; add_typ (Typ "v") (Lst [])
;; add_typ (Typ "w") (Lst [])
;; add_typ (Typ "x") (Lst [])
;; add_typ (Typ "y") (Lst [])
;; add_typ (Typ "z") (Lst [])

;; add_typ (Typ "addr") (Lst [])
(*----------------------------------------------------------------------------*)

let define_type k v line =
	match k with
	| Typ t  ->  if has_typ k
				then warn "type is already defined" k line
				else add_typ k v
	| _  ->  failwith "define_type: typ expected"

let check_docstring s line =
	let check t =
		if has_typ t
			then ()
			else warn "type not defined" t line
	in
	let rec loop = function
		| Typ t :: ts  ->  ( check (Typ t); loop ts )
		| Lst l :: ts  ->  ( loop l;  loop ts)
		| _  :: ts     ->  failwith "check_docstring: strange item found in stack-comment"
		| []  ->  ()
	in
	let pds = parse_docstring s in
	match pds with
	| Lst [Lst intyp; Lst outtyp; Str text]  ->  loop (intyp @ outtyp)
	| _  ->  failwith "check_docstring: parse_docstring returned unexpected things"

let print_all_types () =
	let pairs = 
		Hashtbl.fold (fun k v l -> (k,v) :: l) types_table []
	in
	iter (fun (k,v)  ->  print_string (show_typ k v) ) (sort compare pairs)

(*==========================================================================*)
(*    :section:   instr   *)
(*--------------------------------------------------------------------------*)
let defined_instructions   = ref []
let defined_deinstructions = ref []


let define_instr name (Lst is) =
	defined_instructions := (name, is) :: !defined_instructions;
	()


let get_defined_instr name =
	try (true, assoc name !defined_instructions) with
	| Not_found _  ->  (false, [])

let rec expand_defined_instructions i (Lst ts) =
	let rec recur x =
		if i > 1000 then ccs [] "expand_defined_instructions: recursive definition?";
		let y = unlst (expand_defined_instructions (i+1) (Lst x)) in
		if x <> y then recur y
		else y
	in
	let rec loop =
	function
	| Sym s :: ts  ->
		let (b, x) = get_defined_instr s in
		if not b
		then	Sym s  :: loop ts
		else	(recur x) @ loop ts 
	| t :: ts  ->  t :: loop ts
	| []  ->  []
	in
	Lst (loop ts)

(*===========================================================================*)
(*   :section:   post-rewriting  *)
(*---------------------------------------------------------------------------*)
let post_rewrites = Hashtbl.create 100

let post s =
	if Hashtbl.mem post_rewrites s
	then let c = Hashtbl.find post_rewrites s in Hashtbl.replace post_rewrites s (c+1)
	else Hashtbl.add post_rewrites s 1

let show_post_rewrites () =
	let l = Hashtbl.fold (fun k v l -> (k,v) :: l) post_rewrites [] in
	let rec loop = function
		| []  ->  []
		| (k,v) :: l  ->  (string_justify_right 6 (string_of_int v) ^ "   "^ k) :: loop l
	in
	String.concat "\n" (loop l)

let at_label ts lbl =
	let rec loop = function
		| Label l :: Label l2 :: ts when lbl = l  ->  ts
		| Label l :: ts when lbl = l  ->  ts
		| t :: ts  ->  loop ts
		| []  ->  ccs [] ("at_label: label not found: " ^ lbl)
	in
	loop ts


let post_rewriting (Lst ts) =
	let rec loop all = function
		| [] -> []
		| Sym "go" :: Dest d :: Label l :: ts when d = l
					            ->  post "go &d d:";    loop all (Label l :: ts)

		| Sym "go" :: Dest d :: rest        ->
			( match at_label all d with
			| Sym "ret"           :: ts ->  post "go ret";      loop all (Sym "ret" :: rest)
			| Sym "go" :: Dest nd :: ts ->  post "go go"; Sym "go" :: Dest nd :: loop all rest
			| _                         ->                Sym "go" :: Dest  d :: loop all rest
			)
  		| Sym "rpop"  :: Sym "rpush" :: ts  ->  post "rpop rpush";  loop all ts  
		| Sym "rpush" :: Sym "rpop"  :: ts  ->  post "rpush rpop";  loop all ts
		| Sym "drop"  :: Sym "drop"  :: ts  ->  post "drop drop";   loop all (Sym "drop2" :: ts)
		| Sym "swap"  :: Sym "swap"  :: ts  ->  post "swap swap";   loop all ts
		| Sym "spop"  :: Sym "spush" :: ts  ->  post "spop spush";  loop all ts
		| Sym "spush" :: Sym "spop"  :: ts  ->  post "spush spop";  loop all ts
		| Sym "incw"  :: Sym "incw"  :: ts  ->  post "incw incw";   loop all (Sym "inc2w" :: ts)
		| t :: ts  ->  t :: loop all ts
	in
	let (Lst ts) =
		(Lst ts)
		|> (expand_defined_instructions 0)
		|> fun (Lst ts)  ->  Lst(loop ts ts)
	in
	Lst (loop ts ts)

(*===========================================================================*)
(*   :section:   parts at tail of bytecode: strings, functions  *)
(*---------------------------------------------------------------------------*)
let tail_parts__  = ref []

let print_tail_parts () =
	ps "tail_parts:";
	iter (fun l -> print_item "tail_parts:" (Lst l)) !tail_parts__

let put_at_tail (Lst l) = tail_parts__ := l :: !tail_parts__

let put_fn_at_tail fn =
	let fn_ = gensym() in
	put_at_tail (Lst (Label fn_ :: fn @ [Sym "ret"]));
	fn_
	
let tail_parts () =
	if false then print_tail_parts();
	concat (rev !tail_parts__)

(*==========================================================================*)
(*    :section:   slots   *)
(*--------------------------------------------------------------------------*)
let slot_floor  = ref Int32.zero

let rec read_slotFloor (Int i) =
	if not (mem i [Int32.of_int 16384; Int32.of_int 16777216]) then warn "slotFloor is not one of (16384 16777216)\n" (Lst []) !currentline;
	if !slot_floor = Int32.zero then
		slot_floor := i
	else
		if !slot_floor <> i then ccs  [] "slotFloor: two different specifications found.";
	()

let slot_list__ = ref []

let note_slot label size =   slot_list__ := (label, size) :: !slot_list__

let coded_slots () =
	let rec loop = function
		| []  ->  []
		| (Label s, Int i) :: slts  ->  Label s :: Space (Int32.to_int i) :: (loop slts)
	in
	let out = loop (rev !slot_list__) in
		if false then print_item "coded_slots" (Lst out);
		out

(*==========================================================================*)
let show_ir (Lst ts) =
	let one = Int32.one in
	let spaces s =
		let len = 16 - (String.length s) in
		if len > 0 then (String.make len ' ') else " "
	in
	let append slots=
		let rec loop = function
			| (Label l)as t :: Space i :: slots
				->  nl  :: (show_item t) :: (spaces l) :: "slot   "
					:: (string_of_int i)  :: loop slots
			| x :: slots  ->  failwith "show_ir/append slots: something went wrong."
			| []  ->  []
			in
		String.concat "" (loop slots)
	in
	let rec loop = function
		| []  ->  [nl; nl]
		| (Label s)as t :: ts  ->  nl  :: (show_item t):: (spaces s) :: loop ts
		| Str s :: ts  ->  ("&" ^ show_item (Str s)) :: loop ts
		| Sym "`" :: Sym s :: ts  ->  ("`" ^ s ^ " ") :: loop ts  (* for sugar from _types *)
		| Sym "call" :: Dest d :: ts  ->  (" "^ d ^" ") :: loop ts
		| Word one :: Dest d :: ts  ->  ("`" ^ d ^ " ") :: loop ts  (* for replaced strings  *)
		| Word one :: Word i :: ts  ->  (" "  ^ Int32.to_string i ^ " " ) :: loop ts  (* for char or immediately placed ints *)
		| t :: ts  ->  (show_item t) :: loop ts 
	in
"; ====  IR  ==== -*- mode: toffee -*- *** Automatically created by antoffee\n\n"
	^ "slotFloor   " ^ Int32.to_string !slot_floor ^nl ^nl
	^ (String.concat "" (loop (without_lines ts)))
	^ (append (coded_slots()))
	^ nl
(*==========================================================================*)
