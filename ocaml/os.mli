(* os.mli  *)
(** Standard return type for operation system (OS) interaction. *)

type returned =
	| Line of string
	| Lines of string list
	| Str of string
	| Size of int
	| Ok
	| Failed of string * string			(** name text *)
	
