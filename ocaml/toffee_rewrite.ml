(* toffee_rewrite.ml *)

open List
open Lib
open Toffee_types

(*=====================================================================*)

let rec r items = let (Lst ts) = do_rewrite (Lst items) in ts

(*=====================================================================*)
and dsplit ts =
	(*----------------------------------------------------------*)
	(* 	collect ts from | up to closing #, first | and closing # exclusiv
		now: 	pcond: ts of conditional expression
			prest: ts of remaining program *)
	let rec findts cts = function
		| []             ->  (rev cts,[])
		|  Sym "#" :: ts                     ->  (rev cts,ts)
		| (Sym s   :: Docstr d :: _ ) as ts  ->  (rev cts,ts)
		| t :: ts        ->  findts (t::cts) ts
	in
	let (pcond,prest) = findts [] ts in
	if false then print_item "ts found in cond: "(Lst pcond)
	;

	(*----------------------------------------------------------*)
	(*	cond needs test-then-pairs: ((ctst) (cthn))
		now:	pairs:  [ (tst,thn); ...] *)

	let has_others = ref false in
	let with_others () =
		if !has_others then ccs ts "multiple others in one dspilt found";
		has_others := true
	in

	let rec splitto pairs   ctst cthn   collecting_tst =
		let o_tst = [Sym "go"] in
		let cons ctst cthn pairs = (rev ctst, rev cthn) :: pairs in
		function
		| []                                 ->  rev (cons ctst cthn pairs)

		| Sym "others" :: Sym "|" :: ts when collecting_tst   (* others is first and only test *)
                                                     ->  with_others(); splitto pairs   o_tst []         false ts
		| Sym "others" :: Sym "|" :: ts      ->  with_others(); splitto (cons ctst cthn pairs)   o_tst [] false ts
		| Sym "others" :: ts                 ->  ccs ts "dsplit: others in strange position"

		| Sym "|" :: ts when collecting_tst  ->  splitto pairs   ctst []          false ts
		| Sym "|" :: ts                      ->  splitto (cons ctst cthn pairs)   [] [] true ts

		| t       :: ts when collecting_tst  ->  splitto pairs   (t::ctst) cthn   true  ts
		| t       :: ts                      ->  splitto pairs   ctst (t::cthn)   false ts
	in
	let pairs = splitto []   [] []   true   pcond in
	if false then (iter (fun (tst,thn) -> print_item "test: "(Lst tst); print_item "then: " (Lst thn)) pairs)
	;

	(*----------------------------------------------------------*)
	(*	recurse the whole rewriting circus on all tests and then's
		in order to rewrite forms contained in tst- and thn-parts.
		now:	pairs:  [ (tst,thn); ...],
				all tst and thn expanded            *)
	let pairs = map (fun (tst,thn) -> (r tst,r thn)) pairs in
	if false then begin
		lf(); ps "recurse on all pairs------------"; lf();
		(iter (fun (tst,thn) -> print_item "test: "(Lst tst); print_item "then: " (Lst thn)) pairs)
		end
	;

	(*----------------------------------------------------------*)
	(*	rewrite all then-parts
	   (1)	| tst  | a b c        ->  append         goto  done
	   (2)	| tst  | a b c rec    ->  change rec to  goto  loop
	   (3)	| tst  | a b rec c    ->  change rec to  call  loop, append ret
	   (4)	| tst  | a b recur c  ->  change rec to  xcall loop, append ret
		now:	pairs:  [ (tst,thn); ...],
				all tst and thn expanded,
				rec/recur rewritten
			nesting_rec: true if (3) or (4) happend in some then-part
			loop_:  label
			exit_:  label
	    *)
	let nesting_rec = ref false in
	let xrec        = ref false in
	let mygensym    = gensym() in
	let loop_ = "dsplt_begin" ^ mygensym in
	let exit_ = "dspdone" ^ mygensym in
	let rewrite_thn thn =

		let last l = hd(rev l) in
		let remove_lineinfo l = filter (function | Line _ -> false | x -> true) l in

		let has_rec  = mem (Sym "rec"  ) thn in
		let has_xrec = mem (Sym "recur") thn in
		let is_rec_last =
			match has_rec with
			| true   ->  begin try last(remove_lineinfo thn) = Sym "rec" with _ -> failwith "is_rec_last: something went wrong" end
			| false  ->  false
		in
		let rec remove_rec = function      (* rec is last t *)
			| []  ->  failwith "remove_rec: something went wrong"
			| Sym "rec" :: ts  ->  []
			| t :: ts  ->  t :: remove_rec ts
		in
		let rec replace_rec = function     (* rec is not last t *)
			| []  ->  failwith "replace_rec: something went wrong"
			| Sym "rec" :: ts  ->  Sym "call" :: Dest loop_ :: ts
			| t :: ts  ->  t :: replace_rec ts
		in
		let rec replace_xrec = function     (* keyword is recur, fns xcall, xret *)
			| []  ->  failwith "replace_xrec: something went wrong"
			| Sym "recur" :: ts  ->  Sym "`" :: Sym loop_ :: Sym "xcall" :: ts
			| t :: ts  ->  t :: replace_xrec ts
		in
		begin
		if false then (  print_item "rewrite_thn_in: " (Lst thn); pb has_rec; pb is_rec_last; pb has_xrec; lf());
		let out =
			match (has_rec, is_rec_last, has_xrec) with
			| (false,false, false)  ->  ( thn,              [Sym "go"; Dest exit_] )
			| (true, false, false)  ->  ( replace_rec thn,  [Sym "ret"]            )
			| (true, true,  false)  ->  ( remove_rec  thn,  [Sym "go"; Dest loop_] )
			| (false,false, true)   ->  ( replace_xrec thn, []                     )  
			| _  ->  ccs thn "rec and recur in one conditional expression found."
		in
		if false then (let f(l,t) = print_item "rewrite_thn_out: " (Lst (l @ t)) in f out);
		nesting_rec := !nesting_rec || (has_rec && not is_rec_last);
		xrec        := !xrec        || has_xrec;
		out
		end
	in
	let pairs = map (fun (ctst,cthn) -> (ctst, rewrite_thn cthn)) pairs in
	if !nesting_rec && !xrec then ccs [] "rec and recur in different cases of one conditional";

	(*----------------------------------------------------------*)
	(* concat it all
		insert goto's and Labels
		it comes to:   loop_  tst1 tst2 tst3  thn3 thn2 thn1  exit
		now:	out: rewritten ts of conditional expression *)
	let select_tail (h,t) =
		match !xrec with
		| false  ->  t
		| true   ->  [Sym "xret"]
	in
	let rec loop = function
		| []  ->  []

		| (tst,(thn,ext)) :: []  ->
			let l_ = "cnd" ^ gensym() in
			if !has_others
			then 	thn @ select_tail (thn,ext)	(* others: let run into default *)

			else 	tst @ Dest l_			
				:: Sym "go" :: Dest exit_	(* in case all tests failed *)
				:: Label l_ :: thn @ select_tail (thn,ext)

		| (tst,(thn,ext)) :: ps  ->
			let l_ = "cnd" ^ gensym() in
			tst @ Dest l_
			:: loop ps
			@  Label l_ :: thn @ select_tail (thn,ext)
	in
	let out  = Label loop_ ::   loop pairs   @ [Label exit_] in
	(*----------------------------------------------------------*)
	(* wrap it all up in a function in case there was a nesting recursion
		in one of the then-parts and call that function
		now:	out                                         *)
	let wrap_fn ts =
		let fn_ = put_fn_at_tail ts in Sym "call" :: Dest fn_ :: []
	in
	let out  = if !nesting_rec || !xrec then wrap_fn out else out in
	begin 
	if false then print_item "dsplit rewritten: "(Lst out) ;
	(out,prest)
	end

(*=====================================================================*)
and do_rewrite (Lst items) =

	let is_branch s = mem s branch_symbols in

	let see_ l = (	(r l),
			(r [Str (show_item (Lst l))])   )
	in

	let see2_ l =
		let fn_ = put_fn_at_tail (r l) in
		Word one ::  Dest fn_ :: (r [Str (show_item (Lst l))])
	in

	let dip  l = Sym "rpush"                               :: (r l) @ [Sym "rpop"] in
	let dip2 l = Sym "rpush" :: Sym "rpush"                :: (r l) @ [Sym "rpop"; Sym "rpop"] in
	let dip3 l = Sym "rpush" :: Sym "rpush" :: Sym "rpush" :: (r l) @ [Sym "rpop"; Sym "rpop"; Sym "rpop"] in
	let dip4 l = Sym "rpush" :: Sym "rpush" :: Sym "rpush" :: Sym "rpush" :: (r l) @ [Sym "rpop"; Sym "rpop"; Sym "rpop"; Sym "rpop"] in

	let loop_  body =
		let loop__ = "loop" ^ gensym() in
		let done__ = "ldone"^ gensym() in
		Label loop__
		:: (map (	function
				| Sym "continue" -> Dest loop__
				| Sym "exit"     -> Dest done__
				| t -> t)
			body)
		@ Sym "go" :: Dest loop__ :: Label done__ ::[]
	in
	let ifte iftrue iffalse =
		let yes = gensym() in
		let thn = gensym() in
		Dest yes :: iffalse
		@ [Sym "go"; Dest thn; Label yes]
		@ iftrue @ Label thn :: []
	in
	let skip l =
		let s = gensym() in
		Dest s :: l @ [Label s]
	in
	let then_ branch l =
		let skip__ = gensym() in
		Sym (invert_gocond branch) :: Dest skip__ ::   l @ [Label skip__]
	in

	let fn_ fn =
		let fn_ = put_fn_at_tail fn in Word one :: Dest fn_ :: []
	in

	let str_ s =
		let str = gensym() in
		(
		put_at_tail (Lst (Sym "data" :: Label str :: Str s :: Sym "===" :: []));
		[Word one; Dest str]
		)
	in
	let chr_ s =
		match s with
		| s when String.length s = 1  ->  Word one :: Word (Int32.of_int (Char.code (s.[0]))) :: []
		| s  ->  ccs [Str s] "rewrite 'chr' only for strings of length 1"
	in
	let assert_ l i = 
		let (l, strlit) = see_ l in
		(fn_ l) @ [Int i] @ strlit @ [Sym "Test.i1"]
	in
	let rec out_  l =
		let typs = [Sym "int"; Sym "bool"; Sym "str";] in
		let count = length (filter (fun x -> mem x typs) l) in
		let pre = 
		match count with
		| 0 -> []
		| 1 -> []
		| 2 -> [Sym "swap"]
		| 3 -> [Sym "rev"]
		| 4 -> [Sym "spush"; Sym "spush"; Sym "spush"; Sym "spush"; Sym "spop4";  ]
		| _ -> ccs l "out: maximal 4 values."
		in
		let rec loop l =
		match l with
		| []  ->  []
		| Sym "int"  :: ts  ->  Sym  "iprint" 		:: Sym "spc"   	:: loop ts
		| Sym "bool" :: ts  ->  Sym  "Bool.print"	:: Sym "spc"   	:: loop ts
		| Sym "str"  :: ts  ->  Sym  "print" 		:: Sym "spc"	:: loop ts
		| Str s      :: ts  ->  str_ s @ Sym "print" 	:: Sym "spc" 	:: loop ts
		| Sym "/"    :: ts  ->  Sym  "lf" 				:: loop ts
		| Sym "/=="  :: ts  ->  Sym"lf" :: Sym"`" ::Sym"ddash" :: Sym"print" :: Sym"lf"
										:: loop ts
		| Sym "/--"  :: ts  ->  Sym"lf" :: Sym"`" ::Sym"dash"  :: Sym"print" :: Sym"lf"
										:: loop ts
		| t          :: ts  ->  ccs l ("out: strange thing to do" ^ nl ^ "available types:" ^ show_item (Lst typs))
		in
		let out = loop l in
		if false then ( print_item "pre" (Lst pre); print_item "out" (Lst out); );
		pre @ out
	in

	let fun_ name docstr ts =
		let ok  = function
			| Sym s  ->  not (mem s ["call"; "ret"])
			| Int i  ->  true
			| _      ->  false
		in
		check_docstring docstr !currentline;
		let rec loop f = function
			| []  ->  (rev f, [])
			| (Sym s :: Sym  ":" :: ts) as nts  ->  (rev f, nts)
			| (Sym s :: Docstr d :: ts) as nts  ->  (rev f, nts)
			| t :: ts  ->  loop (t :: f) ts
		in
		let (f, nts) = loop [] ts in
		let f = r f in
		match f with
(*
`foo gets inlined if too short !!!
		| []  ->
			post "inline-idfun";
			Sym "instr" :: Sym name :: (Lst []) :: nts
		| a :: []  when ok a ->
			post "inline-A";
			Sym "instr" :: Sym name :: (Lst f) :: nts
		| a :: b :: [] when ok a && ok b   ->
			post "inline-AB";
			Sym "instr" :: Sym name :: (Lst f) :: nts

		| a :: b :: c :: []  when ok a && ok b && ok c ->
			post "inline-ABC";
			Sym "instr" :: Sym name :: (Lst f) :: nts
*)
		| f  -> ( let f = Sym "===" :: Label name :: f @ [Sym "ret"] in
			put_at_tail (Lst f);
			nts
			)
	in

	let list_ l =
		let rec loop = function
			| []  ->  []
			| Word one :: Dest d :: ts  ->  Word one :: Dest d :: Sym "List.swons" :: loop ts
			| t :: ts  -> t :: Sym "List.swons" :: loop ts
		in
		Int zero :: loop l  @ [Sym "List.reverse"]
	in

	let rec loop ts =
		match ts with
		| []  ->  []
		(*---------------------------------------------------------------------------*)
		| Sym "|" :: ts              ->  let (cond,nts) = dsplit ts               in cond @ loop nts
		| Sym "others" :: ts         ->  let (cond,nts) = dsplit (Sym "go" :: ts) in cond @ loop nts
		| (Sym "#" :: ts) as all     ->  ccs all "'#' found without preceding '|'."
		(*---------------------------------------------------------------------------*)
		| Lst l :: Sym "see"  :: ts   ->  let (l,  strlit) = see_  l in  l  @ strlit @ loop ts
		| Lst l :: Sym "see2" :: ts   ->  see2_ l  @ loop ts
		| Sym "out"  :: Lst l :: ts   ->  out_ l  @ loop ts
		| Lst l :: Int i :: Sym "assert" :: ts  ->  assert_ l i  @ loop ts 
		(*---------------------------------------------------------------------------*)
		| Lst l :: Sym "dip"  :: ts  ->  dip  l @ loop ts
		| Lst l :: Sym "dip2" :: ts  ->  dip2 l @ loop ts
		| Lst l :: Sym "dip3" :: ts  ->  dip3 l @ loop ts
		| Lst l :: Sym "dip4" :: ts  ->  dip4 l @ loop ts

		| Sym "loop" :: Lst l :: ts  ->  loop_ (r l) @ loop ts		(* loop()  	  *)

		| Sym s :: Lst iftrue :: Lst iffalse :: ts when is_branch s	(* gocond()() 	  *)
			  ->  Sym s :: ifte (r iftrue) (r iffalse)  @ loop ts

		| Sym "skip" :: Lst l :: ts  ->  skip  (r l)  @ loop ts		(* gocond skip()  *)

		| Sym s :: Sym "then" :: Lst l :: ts   when is_branch s		(* gocond then()  *)
			  ->  then_ s (r l)  @ loop ts

		| Sym "fn"   :: Lst l :: ts  ->  fn_  (r l)  @ loop ts		(* fn()		  *)
		(*---------------------------------------------------------------------------*)
		| Sym "&"    :: Str s :: ts  ->  Str s  :: loop ts		(* string inline  *)
		| Sym "&"    :: Int i :: ts  ->  Word i :: loop ts		(* &integer       *)
		| Str s               :: ts  ->  str_ s  @ loop ts		(* string at tail *)
		| Sym "chr"  :: Str s :: ts  ->  chr_ s  @ loop ts		(* chr	          *)
		(*---------------------------------------------------------------------------*)
		| Sym "slotFloor" :: Int i :: ts  ->  read_slotFloor (Int i); loop ts
		| Sym "slotFloor" :: ts  ->  ccs ts "slotFloor: integer expeced."

		| Sym "type" :: Sym k :: v :: ts  ->  define_type (Typ k) v !currentline; loop ts

		| Sym "instr" :: Sym name :: Lst l :: ts  ->  define_instr name (Lst l);  loop ts
		| (Sym "instr"   :: rest) as ts  ->  ccs ts "instr: name and instr-list expected."

		| Sym s :: Sym ":" ::Sym "slot" :: Int i :: ts -> note_slot (Label s)(Int i); loop ts

		| Sym s :: Docstr d :: ts    ->  let nts = fun_ s d ts in loop nts	(* function *)

		| Sym s :: Sym ":" :: ts   -> Label s :: loop ts    			(* label: *)
		(*---------------------------------------------------------------------------*)
		| Lst l :: ts  ->  list_  (r l) @ loop ts				(* just a list *)
		(*---------------------------------------------------------------------------*)
		| Line l :: ts  ->  (   currentline := l; Line l :: loop ts  )
		| t :: ts  ->  t :: loop ts
		(*---------------------------------------------------------------------------*)
	in
	Lst( loop items )

let rewrite (Lst items) =
	if false then print_item "enter rewrite with:"(Lst items);
	let out = do_rewrite (Lst items) in
	if false then print_item "leaving rewrite with: " out;
	out
	
(*=====================================================================*)
