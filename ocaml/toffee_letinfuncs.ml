(* toffee_letinfuncs.ml *)

open List
open Lib
open Toffee_types

type name = string
type fn = item list

type func  = name * fn
type funcs = func list
type xfunc = 	| Func  of func
		| Let   of xfunc list
		| In    of xfunc list
		| XFLst of xfunc list


let ps = print_string

let is_dotted name  =  name.[0] = '.'
let dot  a  b       =  a ^"."^ b
let undot     name  = String.sub name 1 (String.length name - 1)

let is_keyword s = mem s [
		"let"; "in"; "funcs";
		"module"; "interface"; "end-of-module";
		]

let is_sep = function
	| Sym ":"   ->  true
	| Docstr d  ->  true
	| _         ->  false

(*==========================================================================*)
(*   :section:   yes!  *)
(*--------------------------------------------------------------------------*)
let sample = XFLst [
	Func ("beg1", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
	Func ("beg2", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
	Let [
		Func ("foo", [Sym "foo"; Lst[Sym "bar"; Sym "baz";]; Sym "bom"; Sym "beg1"; Sym "tail1";] );
		Let [
			Func ("foo", [Sym "foo"; Lst[Sym "bar"; Sym "baz";]; Sym "bom"; Sym "beg1"; Sym "tail1";] );
			Func ("bar", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
		];
		In [
			Func ("baz", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
			Func ("bom", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
		];
		Func ("bar", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
	];
	In [
		Func ("baz", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
		Func ("bom", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
	];
	Func ("tail1", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
	Func ("tail2", [Sym "foo"; Sym "bar"; Sym "baz"; Sym "bom"; Sym "beg1"; Sym "tail1";] );
	]

(*==========================================================================*)
(*   :section:   flatten xfuncs  *)
(* flatten xfuncs gives local functions global names 
 	it needs to return globals before locals in order to preserve
	parent-label before dotted-child-labels
	this is reverse to    let  locals  in  globals  funcs  *)
(*--------------------------------------------------------------------------*)
let show_func (Func (n, fn))  =  n ^ "   " ^ show_item (Lst fn)

let rec print_xfuncs (XFLst xfs)  =
	let rec loop = function
		| []  ->  ()
		| (Func (name, fn)) as f :: fs  ->  (ps (show_func f); lf(); loop fs)
		| Let lfs :: fs  ->  (ps "\nlet...\n"; print_xfuncs (XFLst lfs); ps ""; loop fs)
		| In lfs  :: fs  ->  (ps "in\n";  print_xfuncs (XFLst lfs); ps "funcs\n\n"; loop fs)
	in
	loop xfs

let rec ts_of_funcs = function
	| Func (name, fn) :: fs  ->  Sym name :: fn @ ts_of_funcs fs
	| []  ->  []
	

let flatten_xfuncs  (XFLst xfs) =

    let rec flatten_xfuncs  xfs =
	if false then (ps "======= flatten_xfuncs:\n"; print_xfuncs (XFLst xfs);) ;

	let maprec firstname names f =
		let rec loop = function
			| Sym  x :: ts  when mem x names  ->  Sym  (dot firstname x) :: loop ts
			| Dest x :: ts  when mem x names  ->  Dest (dot firstname x) :: loop ts
			| Lst l :: ts  ->  Lst(loop l) :: loop ts
			| x :: ts  ->  x :: loop ts
			| []  ->  []
		in
		loop f
	in

	let let_in lfs gfs =
		if gfs = [] && lfs <> [] then ccs [] ("let_in_funcs: let something in ??no?? funcs");
		let firstname  =
			match gfs with
			| Func (name,fn) :: fs  -> name
			| []  ->  ""
			in
		let locals     =  map  (function | Func (name,fn) -> name)  lfs in
		let lfs        =  map  (function | 	Func (name,  fn) ->
							Func (dot firstname name,  fn))
					lfs in
		let out        =  gfs @ lfs in		(*  parent-label before dotted-child-label *)
		let out = map  (fun 	(Func (name, fn)) ->
					 Func (name, maprec firstname locals fn ))
				  out
		in
		out
	in

	let rec loop = function
		| Func (name,fn)    :: fs  ->  Func (name,fn) :: loop fs
		| Let lfs :: In gfs :: fs  ->  let_in 	(flatten_xfuncs lfs)
							(flatten_xfuncs gfs)
							@ loop fs
		| []  ->  []
	in

	let outfs =  loop xfs in

    	if false then (ps "\n======= xfuncs flattend:\n";	print_xfuncs (XFLst outfs); );

	outfs
    in

    if false then  (ps "======= flatten_xfuncs:\n"; print_xfuncs (XFLst xfs);) ;
    let outfs = XFLst( flatten_xfuncs xfs ) in
    if false then  (ps "\n======= xfuncs flattend:\n";	print_xfuncs outfs; );
    outfs

(*  let s = print_xfuncs( flatten_xfuncs sample )  *)

(*==========================================================================*)
let let_in_funcs (Lst ts) =
	let flatten lfs gfs =
		match flatten_xfuncs (XFLst [(Let lfs); (In gfs)]) with
		| XFLst fs -> fs
	in

	(*---------------------------------------------------------*)
	let rec collect_body b  =  function
		| (Sym name :: Docstr d :: rest) as ts     ->  (rev b, ts)
		| (Sym name :: Sym ":"  :: rest) as ts     ->  (rev b, ts)
		| (Sym s :: rest) as ts when is_keyword s  ->  (rev b, ts)
		| t :: ts  ->  collect_body (t::b) ts
		| []  ->  (rev b, [])
	in


	let rec collect_childs cs = function
		| Sym name :: x :: ts when (is_sep x) && (is_dotted name)
			->      let (b,nts) = collect_body [] ts in
				collect_childs ((Func (undot name, (x::b))) :: cs) nts
				
		| ts  ->  (rev cs,ts)
	in
	let collect_dotfs = function
		| Sym name :: x :: ts when  not( is_dotted name )

			->  	let (b,nts) = collect_body [] ts in
				let parentf = Func (name, x::b) in
				let (cs,nts) = collect_childs [] nts in
				let fs = flatten cs [parentf] in
				(fs,nts)

		| ts  ->  ccs ts ("undotted name expected.")
	in

	(*---------------------------------------------------------*)
	let rec collect_fs stop fs  = function

		| (Sym name :: x :: rest) as ts when is_sep x

			->  (	let (dotfs,nts) = collect_dotfs    ts in	(* collect dotfs  *)
				collect_fs stop (rev dotfs @ fs) nts		(* recur here     *)
				)

		(* collect fs of complet let-in-funcs struct *)
		| Sym "let" :: ts  -> (	let (ifs,nts) = collect_let ts in
					collect_fs stop ((rev ifs) @ fs) nts
					)

		| Sym stop  :: ts  ->  (rev fs, ts)
		| ts  ->  ccs ts ("let_in_funcs: keyword '" ^stop^ "' expected: ") 

	and collect_let ts =
		let (lfs,nts)  = collect_fs "in"      []  ts in
		let (gfs,nts)  = collect_fs "funcs"   [] nts in
		let fs = flatten lfs gfs  in
		(fs,nts)
	in

	(*---------------------------------------------------------*)
	let collect_module ts =
		let (lfs,nts)  = collect_fs "interface"       []  ts in
		let (gfs,nts)  = collect_fs "end-of-module"   [] nts in
		iter (fun (Func(name,fn)) -> Toffee_inspect.insert_api_func (Lst((Sym name) ::fn))) gfs;
		let fs = flatten lfs gfs in
		(fs,nts)
	in

	(*---------------------------------------------------------*)
	let rec start = function
	| Sym "let"    :: ts  ->  let (fs,nts) = collect_let    ts in ts_of_funcs fs @ start nts
	| Sym "module" :: Sym name :: ts
			->  	let (fs,nts) = collect_module ts in
				let nts =
				match nts with
				| Sym name_2 :: ts when name = name_2  ->  ts
				| (x :: rest) as ts  		       ->   ccs ts ("module '" ^name^ "' does not match end-of-module '" ^(show_item x)^ "'")
				in
				let fs = flatten fs [Func (name,[Docstr "module"])] in
				ts_of_funcs fs @ start nts

	| (Sym "module" :: rest) as ts  ->  ccs ts "module name at begin of module exptected"

	| (Sym name :: x :: rest) as ts when is_sep x
			->  	let (dotfs,nts) = collect_dotfs ts in
				(ts_of_funcs dotfs) @ start nts

	| t :: ts  ->  t :: start ts
	| []  ->  []
	in

	(*---------------------------------------------------------*)
	if false then  (print_item "let_in_funcs in:"   (Lst (without_lines ts)));
	let out = start (without_lines ts) in
	if false then  (print_item "let_in_funcs out: " (Lst (without_lines out)));
	Lst out

(*==========================================================================*)
