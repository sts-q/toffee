(* toffee_toas.ml *)

open List
open Lib
open Toffee_types

(*===========================================================================*)
(*   :section:   toas                                                        *)
(*---------------------------------------------------------------------------*)
let pre =
"# |====^=================================================================^====|
#   :section:   appended
# =============================================================================
	
program:                                # called by base.s as entry point

# =============================================================================
"
(*---------------------------------------------------------------------------*)
let post =
"
# =============================================================================

	ret				# from program

# =============================================================================
#   :section:   appended done
# =============================================================================
"
(*---------------------------------------------------------------------------*)
let program ts =
	let rec loop =  
		let sdec s =
			for i = 0 to (String.length s) - 1 do
				if s.[i] = '%' then s.[i] <- '_' ;
				if s.[i] = '.' then s.[i] <- '_' ;
				if s.[i] = '-' then s.[i] <- '_' ;
				if s.[i] = '?' then s.[i] <- '_' ;
				done;
			
			"p_" ^ s
		in
		let str_ l s =
			let rec loop = function
				| i when i < String.length s  ->  (string_of_int (Char.code s.[i])) :: loop (i+1)
				| i  ->   []
			in
			"\n.section .data\n"
			^ (sdec l) ^ ": \t .int "
			^ (String.concat ", " ((string_of_int (String.length s))
			:: (loop 0)))
			^"\n.section text\n"
		in
		let str_2  s =
			let rec loop = function
				| i when i < String.length s  ->  (string_of_int (Char.code s.[i])) :: loop (i+1)
				| i  ->   []
			in
			"\n.section .data\n"
			^ "\t .int "
			^ (String.concat ", " ((string_of_int (String.length s))
			:: (loop 0)))
			^"\n.section text\n"
		in
		let int_ i =
			"\n.section .data\n"
			^ "\t .int "
			^ (Int32.to_string i)
			^"\n.section text\n"
		in
		let comp   macro arg ts  =  ("\tfvm_"^macro^" \t" ^ arg)   :: loop ts  in
		let simple instr     ts  =  ("\tcall instr_"^instr        )   :: loop ts  in
		let data ts = "\n.section .data" :: loop ts in
		let wall ts = "\n\n.section .text\n\tfvm_wall" :: loop ts in
		function
(*---------------------------------------------------------------------------*)
		| Int i :: ts  ->  ("\tlit " ^(Int32.to_string i)) :: loop ts

(*
		| Word one :: Dest d :: ts  ->  comp "lit_addr" (sdec d) ts
		| Sym "data" :: Label l :: Str s :: Sym "===" :: ts  ->  str_ l s :: loop ts  
		| Label l :: Str s ::  ts  ->  str_ l s :: loop ts
		| Word w :: ts  ->  int_ w :: loop ts

		| Sym "data"  :: ts  ->  data  ts  
		| Sym "==="   :: ts  ->  wall ts
		| Sym "call" :: Dest d :: ts  ->  comp "call" (sdec d) ts
		| Sym "`" :: Sym s :: ts  ->  comp "lit_addr" (sdec s) ts
		| Sym s :: ts  ->  comp "call"   (sdec s)  ts
*)
(*---------------------------------------------------------------------------*)
(*
		| Label l  :: ts  ->  (nl ^ sdec l ^ ":"):: loop ts
		| Dest l   :: ts  ->  failwith ("toas/program: Dest should be done, here: " ^ l ^"\nperhaps a ':' instead of '&'?" )
		| Line l   :: ts  ->  loop ts
*)
		| (x :: ts)as l  ->  failwith( "toas.program: strange item: " ^ (show_item (Lst (take l 10))))
		| []  ->  []
	in
	String.concat nl  (loop ts)

let to_as (Lst ts) =
	pre   ^ "\n\t# ROADWORK\n"   ^ post  

(*===========================================================================*)
