(*  os.ml *)

open List
open Lib

type returned =
	| Line of string
	| Lines of string list
	| Str of string
	| Size of int
	| Ok
	| Failed of string * string
