(* toffee_compile.mli *)
(** Compile AST to bytecode.  *)

open Toffee_types

val to_fp_bytecode: item -> string
