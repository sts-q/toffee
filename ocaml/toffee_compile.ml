(* toffee_compile.ml *)

open List
open Lib
open Toffee_types

(*=====================================================================*)

let compile (Lst items)  =

	let rec loop =

		let to32 = Int32.of_int in

		let simple w ts = Word w :: loop ts in

		let dest  = function
			| Dest d :: ts -> Dest d :: (loop ts)
			| Word d :: ts -> Word d :: (loop ts)
			| ts  ->  ccs ts "compile: Destination or address at gocond expected."
		in
		function
		| []                    ->  [Word (to32 0)]
		(*-----------------------------------------------------*)
		| Sym "==="      :: ts  ->  Word  (to32 0) :: loop ts
		| Sym "call"     :: ts  ->  Word  (to32 2) :: dest ts   
		| Sym "go"       :: ts  ->  Word  (to32 3) :: dest ts
		| Sym "go[>0]"   :: ts  ->  Word  (to32 4) :: dest ts
		| Sym "go[>=0]"  :: ts  ->  Word  (to32 5) :: dest ts
		| Sym "go[==0]"  :: ts  ->  Word  (to32 6) :: dest ts
		| Sym "go[!=0]"  :: ts  ->  Word  (to32 7) :: dest ts
		| Sym "go[<=0]"  :: ts  ->  Word  (to32 8) :: dest ts
		| Sym "go[<0]"   :: ts  ->  Word  (to32 9) :: dest ts
		| Sym "go[>]"    :: ts  ->  Word (to32 10) :: dest ts
		| Sym "go[>=]"   :: ts  ->  Word (to32 11) :: dest ts
		| Sym "go[==]"   :: ts  ->  Word (to32 12) :: dest ts
		| Sym "go[!=]"   :: ts  ->  Word (to32 13) :: dest ts
		| Sym "go[<=]"   :: ts  ->  Word (to32 14) :: dest ts
		| Sym "go[<]"    :: ts  ->  Word (to32 15) :: dest ts
		| Sym "go>0"     :: ts  ->  Word (to32 16) :: dest ts
		| Sym "go>=0"    :: ts  ->  Word (to32 17) :: dest ts
		| Sym "go==0"    :: ts  ->  Word (to32 18) :: dest ts
		| Sym "go!=0"    :: ts  ->  Word (to32 19) :: dest ts
		| Sym "go<=0"    :: ts  ->  Word (to32 20) :: dest ts
		| Sym "go<0"     :: ts  ->  Word (to32 21) :: dest ts
		| Sym "go>"      :: ts  ->  Word (to32 22) :: dest ts
		| Sym "go>="     :: ts  ->  Word (to32 23) :: dest ts
		| Sym "go=="     :: ts  ->  Word (to32 24) :: dest ts
		| Sym "go!="     :: ts  ->  Word (to32 25) :: dest ts
		| Sym "go<="     :: ts  ->  Word (to32 26) :: dest ts
		| Sym "go<"      :: ts  ->  Word (to32 27) :: dest ts
		| Sym "reador"   :: ts  ->  Word (to32 28) :: dest ts
		| Sym "writor"   :: ts  ->  Word (to32 29) :: dest ts
		| Sym "tracor"   :: ts  ->  Word (to32 30) :: dest ts
		| Sym "getor"    :: ts  ->  Word (to32 31) :: dest ts
		| Sym "putor"    :: ts  ->  Word (to32 32) :: dest ts
		| Sym "readorb"  :: ts  ->  Word (to32 33) :: dest ts
		| Sym "writorb"  :: ts  ->  Word (to32 34) :: dest ts
		| Sym "tracor"   :: ts  ->  Word (to32 35) :: dest ts
		| Sym "getor"    :: ts  ->  Word (to32 36) :: dest ts
		| Sym "putor"    :: ts  ->  Word (to32 37) :: dest ts
		(*-----------------------------------------------------*)
		(* free *)
		(*-----------------------------------------------------*)
		| Sym "ret"      :: ts  ->  simple (to32 145) ts
		| Sym "invoke"   :: ts  ->  simple (to32 146) ts
		| Sym "[invoke]" :: ts  ->  simple (to32 147) ts
		| Sym "fly"      :: ts  ->  simple (to32 148) ts
		| Sym "swap"     :: ts  ->  simple (to32 149) ts
		| Sym "over"     :: ts  ->  simple (to32 150) ts
		| Sym "rot"      :: ts  ->  simple (to32 151) ts
		| Sym "tor"      :: ts  ->  simple (to32 152) ts
		| Sym "leap"     :: ts  ->  simple (to32 153) ts
		| Sym "nip"      :: ts  ->  simple (to32 154) ts
		| Sym "tuck"     :: ts  ->  simple (to32 155) ts
		| Sym "rev"      :: ts  ->  simple (to32 156) ts
		| Sym "rpush"    :: ts  ->  simple (to32 157) ts
		| Sym "rpop"     :: ts  ->  simple (to32 158) ts
		| Sym "drop"     :: ts  ->  simple (to32 159) ts
		| Sym "drop2"    :: ts  ->  simple (to32 160) ts
		| Sym "drop3"    :: ts  ->  simple (to32 161) ts
		| Sym "drop4"    :: ts  ->  simple (to32 162) ts
		| Sym "dup"      :: ts  ->  simple (to32 163) ts
		| Sym "dup2"     :: ts  ->  simple (to32 164) ts
		| Sym "dup3"     :: ts  ->  simple (to32 165) ts
		| Sym "dup4"     :: ts  ->  simple (to32 166) ts
		| Sym "hold"     :: ts  ->  simple (to32 167) ts
		| Sym "hold2"    :: ts  ->  simple (to32 168) ts
		| Sym "hold3"    :: ts  ->  simple (to32 169) ts
		| Sym "hold4"    :: ts  ->  simple (to32 170) ts
		| Sym "speek"    :: ts  ->  simple (to32 171) ts
		| Sym "speek2"   :: ts  ->  simple (to32 172) ts
		| Sym "speek3"   :: ts  ->  simple (to32 173) ts
		| Sym "speek4"   :: ts  ->  simple (to32 174) ts
		| Sym "spush"    :: ts  ->  simple (to32 175) ts
		| Sym "spush2"   :: ts  ->  simple (to32 176) ts
		| Sym "spush3"   :: ts  ->  simple (to32 177) ts
		| Sym "spush4"   :: ts  ->  simple (to32 178) ts
		| Sym "spop"     :: ts  ->  simple (to32 179) ts
		| Sym "spop2"    :: ts  ->  simple (to32 180) ts
		| Sym "spop3"    :: ts  ->  simple (to32 181) ts
		| Sym "spop4"    :: ts  ->  simple (to32 182) ts
		| Sym "dec"      :: ts  ->  simple (to32 183) ts
		| Sym "decw"     :: ts  ->  simple (to32 184) ts
		| Sym "dec2w"    :: ts  ->  simple (to32 185) ts
		| Sym "inc"      :: ts  ->  simple (to32 186) ts
		| Sym "incw"     :: ts  ->  simple (to32 187) ts
		| Sym "inc2w"    :: ts  ->  simple (to32 188) ts
		| Sym "@"        :: ts  ->  simple (to32 189) ts
		| Sym "!"        :: ts  ->  simple (to32 190) ts
		| Sym "[@]"      :: ts  ->  simple (to32 191) ts
		| Sym "@b"       :: ts  ->  simple (to32 192) ts
		| Sym "!b"       :: ts  ->  simple (to32 193) ts
		| Sym "[@b]"     :: ts  ->  simple (to32 194) ts
		| Sym "@@"       :: ts  ->  simple (to32 195) ts
		| Sym "@!"       :: ts  ->  simple (to32 196) ts
		| Sym "[@@]"     :: ts  ->  simple (to32 197) ts
		| Sym "@@b"      :: ts  ->  simple (to32 198) ts
		| Sym "@!b"      :: ts  ->  simple (to32 199) ts
		| Sym "[@@b]"    :: ts  ->  simple (to32 200) ts
		| Sym "+"        :: ts  ->  simple (to32 201) ts
		| Sym "-"        :: ts  ->  simple (to32 202) ts
		| Sym "*"        :: ts  ->  simple (to32 203) ts
		| Sym "/"        :: ts  ->  simple (to32 204) ts
		| Sym "%"        :: ts  ->  simple (to32 205) ts
		| Sym "/%"       :: ts  ->  simple (to32 206) ts
		| Sym "[+]"      :: ts  ->  simple (to32 207) ts
		| Sym "[-]"      :: ts  ->  simple (to32 208) ts
		| Sym "[*]"      :: ts  ->  simple (to32 209) ts
		| Sym "[/]"      :: ts  ->  simple (to32 210) ts
		| Sym "[%]"      :: ts  ->  simple (to32 211) ts
		| Sym "[/%]"     :: ts  ->  simple (to32 212) ts
		| Sym "neg"      :: ts  ->  simple (to32 213) ts
		| Sym "abs"      :: ts  ->  simple (to32 214) ts
		| Sym "&&"       :: ts  ->  simple (to32 215) ts
		| Sym "||"       :: ts  ->  simple (to32 216) ts
		| Sym "^"        :: ts  ->  simple (to32 217) ts
		| Sym "[&&]"     :: ts  ->  simple (to32 218) ts
		| Sym "[||]"     :: ts  ->  simple (to32 219) ts
		| Sym "[^]"      :: ts  ->  simple (to32 220) ts
		| Sym "<<"       :: ts  ->  simple (to32 221) ts
		| Sym ">>"       :: ts  ->  simple (to32 222) ts
		| Sym "[<<]"     :: ts  ->  simple (to32 223) ts
		| Sym "[>>]"     :: ts  ->  simple (to32 224) ts
		| Sym "move"     :: ts  ->  simple (to32 225) ts
		| Sym "fill"     :: ts  ->  simple (to32 226) ts
		| Sym "find"     :: ts  ->  simple (to32 227) ts
		| Sym "match"    :: ts  ->  simple (to32 228) ts
		| Sym "moveb"    :: ts  ->  simple (to32 229) ts
		| Sym "fillb"    :: ts  ->  simple (to32 230) ts
		| Sym "findb"    :: ts  ->  simple (to32 231) ts
		| Sym "matchb"   :: ts  ->  simple (to32 232) ts
		| Sym "homio"    :: ts  ->  simple (to32 233) ts
		| Sym "rchan"    :: ts  ->  simple (to32 234) ts
		| Sym "wchan"    :: ts  ->  simple (to32 235) ts
		| Sym "gchan"    :: ts  ->  simple (to32 236) ts
		| Sym "pchan"    :: ts  ->  simple (to32 237) ts
		| Sym "ecode?"   :: ts  ->  simple (to32 238) ts
		| Sym "rcode?"   :: ts  ->  simple (to32 239) ts
		| Sym "rom?"     :: ts  ->  simple (to32 240) ts
		| Sym "ram?"     :: ts  ->  simple (to32 241) ts
		| Sym "map?"     :: ts  ->  simple (to32 242) ts
		| Sym "stdblk?"  :: ts  ->  simple (to32 243) ts
		| Sym "ds?"      :: ts  ->  simple (to32 244) ts
		| Sym "ss?"      :: ts  ->  simple (to32 245) ts
		| Sym "rs?"      :: ts  ->  simple (to32 246) ts
		| Sym "dsn?"     :: ts  ->  simple (to32 247) ts
		| Sym "ssn?"     :: ts  ->  simple (to32 248) ts
		| Sym "rsn?"     :: ts  ->  simple (to32 249) ts
		| Sym "tron"     :: ts  ->  simple (to32 250) ts
		| Sym "troff"    :: ts  ->  simple (to32 251) ts
		| Sym "reset"    :: ts  ->  simple (to32 252) ts
		| Sym "reboot"   :: ts  ->  simple (to32 253) ts
		| Sym "halt"     :: ts  ->  simple (to32 254) ts
		| Sym "data"     :: ts  ->  simple (to32 255) ts
		(*-----------------------------------------------------*)
		| Int i          :: ts  ->  Word one :: Word i  :: loop ts	(* lit  i *)
		| Str s          :: ts  ->  words_of_string s   @  loop ts      (* put string here *)
		| Sym "`" :: Sym s ::ts ->  Word one :: Dest s  :: loop ts	(* lit  &s *)
		| Sym s          :: ts  ->  Word two :: Dest s  :: loop ts	(* call &s *)
		(*-----------------------------------------------------*)
		| Word w         :: ts  ->  Word w  :: loop ts
		| Label l        :: ts  ->  Label l :: loop ts
		| Dest d         :: ts  ->  Dest d  :: loop ts
		(*-----------------------------------------------------*)
		| Line l :: ts  ->  (   currentline := l; loop ts  )
		| ts  ->  ccs ts "Strange item to compile:"
		(*-----------------------------------------------------*)
	in
	if false then print_item "enter compile with: " (Lst items);
	let out = Lst(loop items) in begin
		if false then print_item "leaving compile with: " out;
		out end

(*=====================================================================*)
let count_jumps (Lst ts) =

	let rec loopl pc labels nts = function  
		| []             ->  (labels, rev nts)
		| Label l :: ts  ->  loopl pc         ((l,pc)::labels)  nts        ts
		| Space i :: ts  ->  loopl (pc + i)   labels            nts        ts
		| t       :: ts  ->  loopl (pc + 4)   labels            (t :: nts) ts
	in
	let (labels, nts) = loopl 0          [] [] ts in
	let (labels, nts) = loopl (Int32.to_int !slot_floor) labels (rev nts) (coded_slots()) in  
	if false then print_item "nts" (Lst nts);

	let check_labels =
		let rec loop = function
			| []  ->  ()
			| l :: ls  ->  if mem l ls then error ("label doppelt: " ^ l); loop ls
		in
		loop(map (fun (l,i) -> l) labels)
	in


	let rec loop =
		let find d = try assoc d labels with Not_found -> error ("label not found: " ^ d) in
		function
		| []            ->  []
		| Word w :: ts  ->  Word w   :: loop ts
		| Dest d :: ts  ->  Word (Int32.of_int (find d)) :: loop ts
		| x      :: ts  ->  error ("count_jumps: strange item found: " ^ show_item x)
		in

	Lst (loop nts)  

(*=====================================================================*)
let to_fp_bytecode ts = ts  |> compile  |> count_jumps  |> emit

(*=====================================================================*)
