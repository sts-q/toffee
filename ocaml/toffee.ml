(* rabbit_toffee.ml *)

open List
open Lib

open Toffee_types
open Toffee_letinfuncs
open Toffee_rewrite
open Toffee_compile
open Toffee_toas

(*==========================================================================*)
let get_file_names () =  
		match Command.strings with
		| _ :: names  -> names
		| _   ->  error "No files given."

let get_file name =
	match File.read_string name with
	| Os.Str s  ->  s
	| Os.Failed (name, text) ->  error ("get_file: "
						^ name ^ ":   " ^ text ^ nl)

let get_source_file name = get_file (name ^ ".toffee")

let write_file name s =
	match File.write_string name s with
	| Os.Ok  ->  ()
	| Os.Failed (name, text)  ->  error ("write_file: "
						^ name ^ ":   " ^ text ^ nl)

let write_ir          =  write_file "ir/ir.toffee" 
let write_doc         =  write_file "doc/doc.txt" 
let write_rom_fp      =  write_file "run/rom.fp" 
let write_program_s   =  write_file "asm/fp-x86-program.s"

(*==========================================================================*)
let run () =

	let file_names =  get_file_names () in
	if file_names = [] then
		(print_string "antoffee: Command line empty?.\n"; exit 0;);

	let ir =
		let rec loop = function
			| f :: fs  ->
				( currentfile := f;
				currentline := 0;
				let ts = try parse (get_source_file f) with
					| Failure s ->
						ccs [] ("parsing failed: "^ s)
				in		
				let ts = ts |> let_in_funcs |> rewrite |> unlst in
				ts @ loop fs
				)
			| []  ->  []
		in
		let ts = loop file_names in
		currentfile := "";
		currentline := 0;
		post_rewriting (Lst (ts @ tail_parts()))
	in
	ir |> show_ir         |> write_ir ;

	ir |> Toffee_inspect.ir;  Toffee_inspect.out() |> write_doc;

	ir |> to_fp_bytecode  |> write_rom_fp;

	ir |> to_as           |> write_program_s
		
(*==========================================================================*)

;; try run () with | Compilation_failed | Error -> exit 1

(*==========================================================================*)
