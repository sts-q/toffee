(*    :section:   OCaml *)
(**   parse.ml *)

open List
open Lib


type token = 
        | SimplyList    of token list
        | ParenList     of token list	(* () *)
        | BraketList    of token list	(* [] *)
        | BeginList     of token list   (* begin ... end *)
        | BEGINList     of token list	(* BEGIN ... END *)
        | Word     of string
        | Int      of int32
        | String1  of string	(* "" *)
        | String2  of string	(* '' *)
        | String3  of string	(* {} *)
	| Comment  of string
	| KeyChar  of string	(* :|...  *)
        | Char     of char
        | Newline  of int * int

let rec show_token  =  function
	| SimplyList  l     ->  "(SimplyList: "	^ show_lst1 l     ^ " :SimplyList)"
	| ParenList   l     ->  "(" 		^ show_lst l	  ^ ")"
	| BraketList  l     ->  "[" 		^ show_lst l	  ^ "]"
	| BeginList   l     ->  "(begin: "	^ show_lst l	  ^ " :end)"
	| BEGINList   l     ->  "(BEGIN: "	^ show_lst l	  ^ " :END)"
	| Word w            ->  		  w 	          ^ " "
	| Int i     	    ->  		  Int32.to_string i ^ " "
	| String1 s         ->  "\"" 		^ s               ^ "\" "
	| String2 s         ->  "'" 		^ s               ^ "' "
	| String3 s         ->  "{" 		^ s               ^ "} "
	| Char c            ->  "´" 		^ Char.escaped c  ^ "´ "
	| Newline (l, i)    ->  "<Newline-" ^ string_of_int l ^"-"^ string_of_int i ^ ">"
	| KeyChar s         ->  "<" 		^ s 		  ^ ">"
	| _                 ->  "<<<something else>>>"

and show_lst1  =  function
	| x :: xs  ->  show_token x ^nl^ (show_lst1 xs)
	| []       ->  ""

and show_lst  =  function
	| x :: xs  ->  show_token x ^ (show_lst xs)
	| []       ->  ""


let p1 = SimplyList[	Word "word"; Int (Int32.of_int 7); String1 "string";
			Char 'x';
			Newline (12, 15)]


let keyChars 		= "():|#,"
let numberChars		= "0123456789"
let wordChars		= "[]§$%&\\.~`<>@^/=!?+*-_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
let stringStartChars 	= "{\"'"
let wordAndNumberChars  = numberChars ^ wordChars
let isWordOrNumberChar c = String.contains wordAndNumberChars c

let stringEndChar = function
	| '{'   ->  '}'
	| '"'   ->  '"'
	| '\''  ->  '\''
	| c	->  failwith ("stringEndChar: strange Char" ^ String.make 1 c)

let stringEndFun stringEndChar s =
	match  stringEndChar  with
	| '}'   ->  String3 s
	| '\''  ->  String2 s
	| '"'   ->  String1 s
	| c	->  failwith ("stringEndFun: strange Char" ^ String.make 1 c)

let emacs_pleaseHighlightmytext = '"'


type list_type = | Simply  | Paren   | Braket   | Begin   | BEGIN


(*============================================================================
 :section:   toffee
----------------------------------------------------------------------------*)
let toffee_tokens source =

	let srclen 	= String.length source				in
	let cc i 	= source.[i] 					in
	let substr start ende  = String.sub source start (ende - start) in

	let rec readComment = function
		| i when i = srclen	->  i
		| i when cc i = '\n'	->  i
		| i			->  readComment (i+1)
		in

	let rec readString endChar i1 i2  =
		match  i2  with
		| i2  when i2 = srclen		->  failwith ("string end char missing: '" ^ Char.escaped endChar ^ "'")
		| i2  when cc i2 = endChar	->  (i2+1, stringEndFun endChar (substr i1 i2))
		| i2				->  readString endChar i1 (i2+1)
		in

	let rec readWordOrNumber =
		let wordOrNumber s =
			try
				let i64 = int64_of_signed_string s in
				if    i64 > Int64.of_int32 (Int32.max_int)
				   || i64 < Int64.of_int32 (Int32.min_int)
				then failwith ("number too large: " ^ s)
				else Int (Int64.to_int32 i64)
			with
			| Failure "int_of_string"  ->  Word s
		in
		function
		| i1, i2 when i2 = srclen			->  (i2, wordOrNumber (substr i1 i2))
		| i1, i2 when isWordOrNumberChar (cc i2)	->  readWordOrNumber (i1, i2+1)
		| i1, i2					->  (i2, wordOrNumber (substr i1 i2))
	in

	let is_special_or i  =  cc i  = '|'  &&  i + 1 < srclen  &&  cc (i+1) = '|'
	in

	let rec loop i line ts =
		if  i = srclen  then   rev ts  else

		match  cc i  with
		| c when c = ' '			->  loop (i+1) line ts
		| c when c = '\t'			->  loop (i+1) line ts
		| c when c = '\n'			->  loop (i+1) (line+1) (Newline (line, 0) :: ts)
		| c when c = ';'			->  loop (readComment i) line ts

		| c when String.contains keyChars c && is_special_or i  -> loop (i+2) line ((Word "||") :: ts)
		| c when String.contains keyChars c	->  loop (i+1) line (KeyChar (substr i (i+1)) :: ts)

		| c when String.contains stringStartChars c
				->  (match  readString  (stringEndChar c)  (i+1) (i+1)  with
				     | ni, t  		->  loop ni line (t :: ts)
				     )

		| c when isWordOrNumberChar c
				->  (match  readWordOrNumber (i, i)  with
				    | ni, t     	->  loop ni line (t :: ts)
				    )

		| c		->  failwith ("parse.toffee: strange char found near line " ^ string_of_int line ^":  '" ^ String.make 1 c ^ "'")

	in
	loop 0 2 []    (* first newline-char sends us to line two *)

let toffee_tree  tokens  =

    let rec loop lt ts nts  =			(* lt: list type *)
	match   ts  with
	| []				 ->  if  lt = Simply  then  ([], SimplyList (rev nts)) else failwith "closing ')' end or END missing"

	| t::ts when t = KeyChar "("	 ->  (match  loop Paren  ts []  with | r, l  ->  loop lt r (l :: nts))
	| t::ts when t = KeyChar "["	 ->  (match  loop Braket ts []  with | r, l  ->  loop lt r (l :: nts))
	| t::ts when t = Word "begin"    ->  (match  loop Begin  ts []  with | r, l  ->  loop lt r (l :: nts))
	| t::ts when t = Word "BEGIN"    ->  (match  loop BEGIN  ts []  with | r, l  ->  loop lt r (l :: nts))

	| t::ts when t = KeyChar ")"	 ->  if  lt = Paren   then  ts, ParenList  (rev nts) else failwith "')' found without matching '('"
	| t::ts when t = KeyChar "]"	 ->  if  lt = Braket  then  ts, BraketList (rev nts) else failwith "']' found without matching '['"
	| t::ts when t = Word "end"	 ->  if  lt = Begin   then  ts, BeginList  (rev nts) else failwith "'end' found without matching 'begin'"
	| t::ts when t = Word "END"	 ->  if  lt = BEGIN   then  ts, BEGINList  (rev nts) else failwith "'END' found without matching 'BEGIN'"

	| t::ts 	->  loop lt ts (t::nts)
	in

    match  loop Simply tokens []  with
    | _, nts  ->  nts
	
	

let toffee source = toffee_tree (toffee_tokens source)


(*
;;ps (show_token (rabbit "abghij  123(123 [1 2 3] 4)"));;
*)


(* ======================================================================== *)
(* :section:   docstring  *)
(* ------------------------------------------------------------------------ *)
let docstring s =
	let separator_found char i =  s.[i] = char  &&  s.[i+1] = char in
	let substr     first last  =  String.sub s first (last - first + 1)  in

	let len            =  String.length s  in
	let comment_found  =  ref false in
	let comment_pos    =  ref 0 in
	let typ_string     =  ref "" in
	let text           =  ref "" in

	let rec find_comment = function
		| i when i <= len - 2 && separator_found '/'i  -> (
			comment_found := true;
			comment_pos   := i;
			)
		| i when i <= len - 2  ->  find_comment (i + 1)
		| i  ->  ()
	in
	let rec has_stackcomment_delimiter = function
		| i when i <= len - 2 && separator_found '-' i  -> true
		| i when i <= len - 2  ->  has_stackcomment_delimiter (i + 1)
		| i  ->  false
	in
	find_comment 0;
	(match !comment_found with
	| true   ->  begin
			typ_string := substr 0 (!comment_pos -1);
			text       := substr (!comment_pos + 2) (len - 1);		
			end
	| false ->  begin
			if has_stackcomment_delimiter 0
				then ( typ_string := s;  text := "" )
				else ( typ_string := ""; text := s  )
			end
	);
	SimplyList [toffee !typ_string;  String3 !text]

(*==========================================================================*)
