(** Libra Pro Diversa. *)

val lf: unit -> unit
(** print newline *)

val nl: string
(** newline-string: "\n" *)

val ps: string -> unit
(** print_string with one trailing space char appended. *)

val pb: bool   -> unit
(** print bool  with one trailing space char appended. *)

val pi: int    -> unit
(** print integer  with one trailing space char appended. *)

val drop: 'a list -> int -> 'a list
(** drop first n elements of list l *)

val take: 'a list -> int -> 'a list
(** take first n elements of list l *)

val int64_of_signed_string: string -> int64
(** use Int32.of_string to get integer of string, check for +/- sign *)

val spaces: int -> string -> string
val string_justify_left:  int -> string -> string
val string_justify_right: int -> string -> string
val string_maximal: int -> string -> string

val nextGenname: 	unit -> string
(** return strings like: "1" "2" "3" "4" ...  *)
