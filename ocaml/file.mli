(* file.mli  *)
(** Functions to read and write files to disk. *)

open Os

val exists: string -> bool

val size: string -> Os.returned

val remove: string -> Os.returned

val read_string: string -> Os.returned

val read_lines: string -> Os.returned

val write_string: string -> string -> Os.returned

val write_lines: string -> string list -> Os.returned

val append_line: string -> string -> Os.returned

