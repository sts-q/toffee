(* :section:   OCaml *)
(** Parse a string into a (SimplyList[...]):token. *)

type token = 
        | SimplyList    of token list
        | ParenList     of token list	(* () *)
        | BraketList    of token list	(* [] *)
        | BeginList     of token list	(* begin end *)
        | BEGINList     of token list	(* BEGIN END *)
        | Word     of string
        | Int      of int32
        | String1  of string		(* "" *)
        | String2  of string		(* '' *)
        | String3  of string		(* {} *)
	| Comment  of string
	| KeyChar  of string		(* :|# *)
        | Char     of char
        | Newline  of int * int

val show_token: token -> string


val toffee: string -> token
val docstring: string -> token

