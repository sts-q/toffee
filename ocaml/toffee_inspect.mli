(* toffee_inspect.mli *)
(** Introspection of the compiled code.  *)

open Toffee_types


val ir: item -> unit
(** insert (or record or log) a instruction, just compiled into bytecode. *)

val insert_api_func: item -> unit
(** insert (or record or log) a toffee module api function : Modulename.funcname  *)

val out: unit -> string


