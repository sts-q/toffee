(* toffee_types.mli *)
(** Types show print parse, lib-toffee, docstrings, types, exceptions, slots.  *)


(*--------------------------------------------------------------------------*)
(** {2  Types} *)

type item =	| Sym of string
		| Char of char
		| Str  of string
		| Docstr of string
		| Int of int32
		| Bool of bool

		| Lst of item list

		| Label of string
		| Dest of string
		| Word of int32

		| Line of int 			(** line number  *)
		| Typ  of string
		| Space of int

val zero: int32
val one:  int32
val two:  int32


val parse: string -> item
(** Parse string into (Lst [...]):item. *)

val show_item: item -> string
(**  Show item, separated by one space, strings like 'string': 1 2 'string' 3 *)

val print_item: string -> item -> unit
(**  Print message and item using show_item. *)


(*--------------------------------------------------------------------------*)
(** {2  lib_toffee} *)

val currentline: int ref
val currentfile: string ref

val warn: string -> item -> int -> unit
(** print a message *)

val section: string -> string -> string
(** make section string for automatically created toffee files *)

val gensym: unit -> string
(** generate next unique symbol: 1 2 3 4 5 ... *)

val branch_symbols: string list
(** go>0 go<=0 .... go[<] *)

val invert_gocond: string -> string
(** go>0  --> go<=0 *)

val words_of_string: string -> item list
(** words_of_string s ==> Word string-length :: Word (chrcode s.[0]) :: Word (chrcode s.[1]) ... :: [] *)

val unlst: item -> item list

val without_lines: item list -> item list
(** remove (Line i) items from item list *)

val emit: item -> string
(** convert a list of 32-bit-integers to string *)

(*--------------------------------------------------------------------------*)
(** {2  Exceptions} *)

exception Compilation_failed
val ccs: item list -> string -> 'a
(** can't compile source: something went wrong compiling the given source. *)

exception Error
val error: string -> 'a
(** Error: something went wrong reading or writing files, os-dirs, others than compiling. *)

(*--------------------------------------------------------------------------*)
(** {2  docstrings} *)

val parse_docstring: string -> item

(*--------------------------------------------------------------------------*)
(** {2  types} *)

val define_type:     item -> item -> int -> unit

val check_docstring: string  -> int -> unit
(** Check if all types, used in docstring, are defined. Print a message if not. *)

val print_all_types: unit -> unit

(*--------------------------------------------------------------------------*)
(** {2  instr} *)

val define_instr: string -> item -> unit
(** define a new instruction in terms of existing ones: a simple macro *)

(*--------------------------------------------------------------------------*)
(** {2  post-rewriting} *)

val post: string -> unit

val post_rewriting: item -> item

val show_post_rewrites: unit -> string

(*--------------------------------------------------------------------------*)
(** {2  tail parts} *)

val put_at_tail: item -> unit

val put_fn_at_tail: item list -> string
(** put item list with 'Sym ret' joined as fn at tail, return gensym fn_ *)

val tail_parts: unit -> item list

(*--------------------------------------------------------------------------*)
(** {2  slots} *)

val read_slotFloor: item -> unit
(** read slotFloor and remember *)

val slot_floor: int32 ref

val note_slot: item -> item -> unit
(** Remember slot for later, don't compile it into bytecode. *)

val coded_slots: unit -> item list
(** Return list of remembered slots. *)

(*--------------------------------------------------------------------------*)
(** {2 ir} *)

val show_ir: item -> string
(** show intermediate representation *)

