(*  lib.ml*)

open List

let lf () = print_newline()  
let nl = "\n"                
let ps str = print_string (str                  ^" ");flush stdout      
let pb b   = print_string (if b then "true " else "false ")
let pi i   = print_string ((string_of_int i)    ^" ")

(* drop n elements of l. Return [] if n > (length l)  *)
let rec drop  l n =
    match n with
    | 0   -> l
    | _   -> (match l with
        	| x::xs   ->  drop xs (n-1)
        	| []      ->  [])

(* take n elements of l. Return [] if n > (length l)  *)
let rec take l n =
	match n with
	| 0	->  []
	| _	->  (match l with
		     |  x :: xs  ->  x :: take xs (n-1)
		     |  []       ->  [])


let int64_of_signed_string s =
	let len = String.length s in
	if String.length s = 0 then raise (Failure "int_of_string");
	let c0 = s.[0] in
	if len = 1 && (c0 = '-' || c0 = '+') then raise (Failure "int_of_string");
	match c0 with
	| '+'  ->    Int64.of_string (String.sub s 1 (len-1))
	| '-'  -> Int64.mul ( Int64.of_string (String.sub s 1 (len-1)))  Int64.minus_one
	| _    ->   Int64.of_string  s


let spaces i s = String.make (max 0 (i - String.length s)) ' '
let string_justify_left  i s = s ^  spaces i s
let string_justify_right i s =      spaces i s  ^ s
let string_maximal i s = if String.length s > i then String.sub s 0 i else s

(*----   :section:   Genname -----------------------------------------------*)
let gennameCount 	= ref 0

let currentGencount () 	= !gennameCount

let nextGencount () 	= incr gennameCount; !gennameCount

let currentGenname  () 	= "N" ^string_of_int (currentGencount())


let nextGenname  () 	= string_of_int (nextGencount())

(*==========================================================================*)

