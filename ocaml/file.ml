(*  file.ml *)

open List
open Lib

let write_flags = [Unix.O_WRONLY; Unix.O_CREAT; Unix.O_TRUNC]
let append_flags = [Unix.O_WRONLY; Unix.O_APPEND; Unix.O_CREAT]
let read_flags = [Unix.O_RDONLY]

let permissions = 0o666

let show_error err fn param =
	"Unix.error: " ^ Unix.error_message err ^": "^ fn ^": "^ param



let exists path = Sys.file_exists path

let size path =
	try
		let stats = Unix.stat path in
		Os.Size stats.st_size
	with
	| Unix.Unix_error (err,f,p)   ->   Os.Failed (Unix.error_message err, path)

let remove path =
	try
		Sys.remove path;
		Os.Ok
	with
	| Sys_error s   ->   Os.Failed (s, path)


let read_string path =
	try
		let fd = Unix.openfile path read_flags permissions in
		let size = (Unix.fstat fd).st_size in
		let buff = String.create size in
		try
			let len = Unix.read fd buff 0 size in
			if len = size then
				Os.Str buff
			else
				Os.Failed ("File.read_string: len <> size, (currently maximal 64kB)\n", path)
		with
		| Unix.Unix_error (err,f,p)  ->  Os.Failed (Unix.error_message err, path)
	with
	| Unix.Unix_error (err,f,p)  ->  Os.Failed (Unix.error_message err, path)


let read_lines path  =
	try
		let ic = open_in path in
        	let rec read ls =
                	try
	                    	let line = input_line ic in
        	            	read (line :: ls)
	                with
				End_of_file -> close_in ic; rev ls
                in
               	Os.Lines (read [])
	with
	| Sys_error s -> Os.Failed (s, path)


let write path flags append_nl ls =
	try
		let fd = Unix.openfile path flags permissions in
		let rec write_lines = function
			| []   ->   ()
			| l :: ls   ->
				let s = if append_nl then l ^ nl else l
				in
				Unix.write fd s 0 (String.length s);
				write_lines ls
		in
		try
			write_lines ls;
			Unix.close fd;
			Os.Ok
		with
		| Unix.Unix_error (err,f,p) ->
			(Unix.close fd;
			Os.Failed (Unix.error_message err, path))
	with
	| Unix.Unix_error (err,f,p)   ->   Os.Failed (Unix.error_message err, path)


let write_string path str  =  write path write_flags false [str]

let write_lines path lines  =  write path write_flags true lines

let append_line path line  =  write path append_flags true [line]


