(* toffee_inspect.ml *)

open List
open Lib
open Toffee_types

(*==========================================================================*)
(* :section:   fvm instructions   *)
(*--------------------------------------------------------------------------*)
let instrs = [
   (*	"===";   *)
   (*	"call";  *)
   	"go";    
   (*	"go[>0]"; "go[>=0]"; "go[==0]"; "go[!=0]"; "go[<=0]"; "go[<0]";
	"go[>]"; "go[>=]"; "go[==]"; "go[!=]" ; "go[<=]"; "go[<]";
	"go>0"; "go>=0"; "go==0"; "go!=0" ; "go<=0"; "go<0";
	"go>"; "go>="; "go==" ; "go!="  ; "go<="   ; "go<"     ;
	 "reador"; "writor" ; "tracor"  ; "getor"; "putor" ;
	"readorb"; "writorb" ; "tracor"; "getor"; "putor";
    *)
	"ret"      ; "invoke"   ; "[invoke]" ; "fly"      ;
	"swap"     ; "over"     ; "rot"      ; "tor"      ;
	"leap"     ; "nip"      ; "tuck"     ; "rev"      ;
	"rpush"    ; "rpop"     ;
	"drop"     ; "drop2"    ; "drop3"    ; "drop4"    ;
	"dup"      ; "dup2"     ; "dup3"     ; "dup4"     ;
	"hold"     ; "hold2"    ; "hold3"    ; "hold4"    ;
	"speek"    ; "speek2"   ; "speek3"   ; "speek4"   ;
	"spush"    ; "spush2"   ; "spush3"   ; "spush4"   ;
	"spop"     ; "spop2"    ; "spop3"    ; "spop4"    ;
	"dec"      ; "decw"     ; "dec2w"    ; "inc"      ; "incw"     ; "inc2w"    ;
	"@"        ; "!"        ; "[@]"      ; "@b"       ; "!b"       ;
	"[@b]"     ; "@@"       ; "@!"       ; "[@@]"     ; "@@b"      ; "@!b"      ; "[@@b]"    ;
	"+"        ; "-"        ; "*"        ; "/"        ; "%"        ; "/%"       ;
	"[+]"      ; "[-]"      ; "[*]"      ; "[/]"      ; "[%]"      ; "[/%]"     ;
	"neg"      ; "abs"      ; "&&"       ; "||"       ; "^"        ;
	"[&&]"     ; "[||]"     ; "[^]"      ; "<<"       ; ">>"       ; "[<<]"     ; "[>>]"     ;
	"move"     ; "fill"     ; "find"     ; "match"    ;
	"moveb"    ; "fillb"    ; "findb"    ; "matchb"   ;
	"homio"    ;
	"rchan"    ; "wchan"    ; "gchan"    ; "pchan"    ;
	"ecode?"   ; "rcode?"   ;
	"rom?"     ; "ram?"     ; "map?"     ; "stdblk?"  ;
	"ds?"      ; "ss?"      ; "rs?"      ; "dsn?"     ; "ssn?"     ; "rsn?"     ;
	"tron"     ; "troff"    ;
	"reset"    ; "reboot"   ; "halt"     ;
  (*	"data"     ;  *)
	]

(*==========================================================================*)
(* :section:   compiled instructions   *)
(*--------------------------------------------------------------------------*)
let compis  = Hashtbl.create 500  (* compiled instructions *)
let insequs = Hashtbl.create 500  (* in-sequence instructions *)

let ici t =   (* insert compiled instruction *)
	if Hashtbl.mem compis t then
		let count = Hashtbl.find compis t in
		Hashtbl.replace compis t (count + 1)
	else
		Hashtbl.add compis t 1


let iis t =   (* insert in-sequence *)
	if Hashtbl.mem insequs t then
		let count = Hashtbl.find insequs t in
		Hashtbl.replace insequs t (count + 1)
	else
		Hashtbl.add insequs t 1




let ir (Lst ts) =
	let a = ref "" in
	let b = ref "" in
	let seq c =
		if mem c instrs then (
			if    mem !a instrs  &&  mem !b instrs	then
				iis (!a ^" "^ !b ^" "^ c);
			a := !b;
			b :=  c;
		) else (
			b := "";
		)
	in
	let rec loop  = function
		| Sym s :: ts  ->  ici (Sym s); seq s;		loop ts
		| Int i :: ts  ->  ici (Sym "lit"); 		loop ts
		| Str s :: ts  ->  ici (Sym "\"some string\""); loop ts
		| _ :: ts      ->  loop ts
		| []  ->  ()
	in
	loop ts 

let instruction_frequencies () =

	let fmt (k, v) = string_justify_right 6 (string_of_int v) ^"   "^ show_item k in

	let cis = Hashtbl.fold (fun k v l -> (k,v) :: l) compis [] in
	let cis1 = map fmt   
		  (sort (fun (k1,v1) (k2,v2) -> compare k1 k2)
		   cis)
	in
	let cis2 = map fmt
		  (sort (fun (k1,v1) (k2,v2) -> (compare v1 v2) * -1)
		   cis)
	in
	let rec lines cis1 cis2 =
		match cis1,cis2 with
		| c :: cs, d :: ds  ->  (c ^ spaces 30 c ^ d) :: lines cs ds
		| [], []  ->  []
	in
	String.concat nl (lines cis1 cis2)
	^nl
	^nl
	^ string_of_int (length cis1) ^" different symbols found." ^nl
	^ string_of_int (fold_left (fun c (k,v) -> v + c) 0 cis) ^ " symbols counted."


let instruction_sequences () =

	let fmt (k,v) = string_justify_right 6 (string_of_int v) ^"   "^ k in

	let seqs = Hashtbl.fold (fun k v l -> (k,v) :: l) insequs [] in
	let seqs = sort (fun (k1,v1) (k2,v2) -> (compare v1 v2) * -1) seqs in
	String.concat nl (map fmt seqs)

(*==========================================================================*)
(* :section:   Toffee Module apis  *)
(*--------------------------------------------------------------------------*)
let funcs = ref []

let insert_api_func (Lst func) =
	match func with
	| Sym name :: Docstr d :: body ->  funcs := (name,Docstr d) :: !funcs
	| Sym name :: Sym ":" :: body  ->  funcs := (name^":",Sym "" ) :: !funcs
	| func  ->  failwith "no func found in inset_api_func: " (Lst func)
	;
	()

let module_apis() =
	let fmt (name,d) = string_justify_left 20 name ^ show_item d in
	let rec loop = function
		| f :: fs  ->  fmt f :: loop fs
		| []  ->  []
		| _  ->  failwith "Toffee_inspect.module_apis: something went wrong."
	in
	String.concat nl (loop !funcs)
	^nl



(*==========================================================================*)
(* :section:   Toffee Module apis  *)
(*--------------------------------------------------------------------------*)
let out () = 
"; ====  DOC  === -*- mode: toffee -*- *** Automatically created by antoffee\n\n"
(*  ^ section "Module api"              "toffee" ^ (module_apis()) ^nl  *)
^ section "post-rewrites" "toffee" ^ (show_post_rewrites ()) ^nl^nl
^ section "instruction frequencies" "toffee" ^ (instruction_frequencies ()) ^nl^nl
^ section "instruction sequences"   "toffee" ^ (instruction_sequences ()) ^nl^nl
^nl
^ "; ==========================================================================="
^nl

(*==========================================================================*)

