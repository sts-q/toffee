# |====^=================================================================^====|
#   :section:   appended
# =============================================================================
	
program:                                # called by base.s as entry point

# =============================================================================
	fvm_lit_addr 	p___3
	fvm_call 	p_print
	fvm_call 	p_lf
	fvm_halt

p_iofail:
	fvm_halt

p_drop2_return:
	fvm_drop
	fvm_drop
	fvm_ret


.section .text
	fvm_wall

p_cprint:
	fvm_cprint 	
	fvm_ret


.section .text
	fvm_wall

p_lf:
	fvm_lit 	10
	fvm_call 	p_cprint
	fvm_ret


.section .text
	fvm_wall

p_times:

p_loop__1:
	fvm_brlez 	p_drop2_return
	fvm_rpush
	fvm_dup
	fvm_rpop
	fvm_rpush
	fvm_rpush
	fvm_invoke
	fvm_rpop
	fvm_rpop
	fvm_dec
	fvm_jmp   	p_loop__1

p_ldone__2:
	fvm_ret


.section .text
	fvm_wall

p_print:
	fvm_load__
	fvm_lit_addr 	p_print_next
	fvm_swap
	fvm_call 	p_times
	fvm_drop
	fvm_ret


.section .text
	fvm_wall

p_print_next:
	fvm_incw
	fvm_load__
	fvm_call 	p_cprint
	fvm_ret

.section .data
p___3: 	 .int 12, 104, 101, 108, 108, 111, 32, 119, 111, 114, 108, 100, 33
.section text

# =============================================================================

	ret				# from program

# =============================================================================
#   :section:   appended done
# =============================================================================
