# =============================================================================
# :section:   constants definitions
# -----------------------------------------------------------------------------
.section .data
	
.equ	SYS_EXIT, 1
.equ	SYS_WRITE, 4
.equ	STDOUT, 1
.equ	WORD_SIZE, 4
.equ	dw, 4				# delta w: word_size
	
hello_message:		.asciz       "hello world, this is native! |"   # len: 30
bye_message:		.asciz       "bye! (native done)            "
hit_data_message:	.asciz       "bye! (HIT DATA   )            "
hit_wall_message:	.asciz       "bye! (HIT WALL   )            "
hit_roadwork:		.asciz       "bye! (HIT ROADWORK)           "

# =============================================================================
#   :section:   Register
# -----------------------------------------------------------------------------
.equ	tos,	%eax
.equ	left, 	%ebx			# second operand: 5 3 - => 2 -->  4:ebx 3:eax - = 2:eax
					# ecx
					# edx
.equ	ssp,	%esi
.equ	dsp,	%edi
	
# =============================================================================
# :section:   RAM variables
# -----------------------------------------------------------------------------
.section .bss				# bss: RAM, zeroed at start (as)
	
.lcomm  ds,  256 * 4
	
.lcomm  ss,  256 * 4
	
.lcomm  buffer, 256 * 4
# =============================================================================
# :section:   programm begins here
# -----------------------------------------------------------------------------
.section .text
	
.globl _start
_start:
	movl 	$hello_message, %eax
	call 	print_30
	call	lf
	
	call 	init
	call	program			# call compiled freelang programm
	
	call	lf
	movl 	$bye_message, %eax
	call 	print_30
	call	lf
	jmp	exit

# -----------------------------------------------------------------------------
init:	
	movl   	$ds, dsp		# init DS
	addl	$1024, dsp
	xorl	tos, tos
	xorl	left, left
					
	movl   	$ss, ssp		# init SS
	addl	$1024, ssp
	
	ret
	
# =============================================================================
#   :section:   lib
# -----------------------------------------------------------------------------
exit:	
	movl	$SYS_EXIT, %eax
	xorl	%ebx, %ebx		# exitcode: 0
	int	$0x80


print_eax:
	movl	%eax, (buffer)
	movl	$SYS_WRITE, %eax
	movl	$STDOUT, %ebx	
	movl	$buffer, %ecx	
	movl	$4, %edx	
	int	$0x80
	ret

lf:
	movl	$10, %eax
	call	print_eax
	ret

print_30:  #  eax: buffer
	movl	%eax, %ecx
	movl	$SYS_WRITE, %eax		# SYS_write  4
	movl	$STDOUT, %ebx			# fd  fileno(stdio)
	movl	$30, %edx			# count  
	int	$0x80
	ret
	
# =============================================================================
#   :section:   internal macros 
# -----------------------------------------------------------------------------
.macro roadwork err
	call lf
	movl $hit_roadwork, %eax
	call print_30
	call lf
	movl \$err, %eax
	print_eax
	call lf
	jmp exit
   .endm
# -----------------------------------------------------------------------------
.macro dspNext
	subl $dw, dsp
    .endm

.macro dspPrev
	addl $dw, dsp
    .endm

.macro tosUp
	movl tos, (dsp)
    .endm

.macro sosDown
	movl (dsp), tos
    .endm

.macro sosLeftDown
	movl (dsp), left
    .endm

.macro leftUp
	movl left, (dsp)
    .endm

# =============================================================================
#   :section:   instruction implemetation
# -----------------------------------------------------------------------------
in_wall:
	call 	lf
	movl	$hit_wall_message, %eax		
	call	print_30
	call 	lf
	jmp 	exit

in_halt:
	call lf
	movl	$bye_message, %eax
	call	print_30
	call lf
	jmp exit

in_data:
	call lf
	movl	$hit_data_message, %eax	
	call	print_30
	call lf
	jmp exit

# =============================================================================
#   :section:   fvm instruction macros
# -----------------------------------------------------------------------------
.macro fvm_string str:req
	.asciz \str
    .endm
# -----------------------------------------------------------------------------
.macro fvm_wall
	jmp in_wall
   .endm
	
.macro fvm_lit value:req
	dspNext
	tosUp
	movl $\value, tos
   .endm

.macro fvm_lit_addr dest:req
	dspNext
	tosUp
	movl $\dest, tos
   .endm

.macro fvm_call dest:req
	call \dest
    .endm
	
# -----------------------------------------------------------------------------
# :section:   compare
# -----------------------------------------------------------------------------
.macro fvm_jmp dest:req			# go
	jmp \dest
    .endm

	
# -----------------------------------------------------------------------------
.macro fvm_brgz dest:req		# go[>0]
	cmpl $0, tos
	jg \dest
    .endm

.macro fvm_brgez dest:req		# go[>=0]
	cmpl $0, tos
	jge \dest
    .endm
	
.macro fvm_brez dest:req		# go[==0]
	roadwork 65
    .endm

.macro fvm_brnz dest:req		# go[!=0]
	roadwork 67
    .endm
	
.macro fvm_brlez dest:req		# go[<=0]
	cmpl $0, tos
	jle \dest
    .endm

.macro fvm_brlz dest:req		# go[<0]
	roadwork 68
    .endm
# -----------------------------------------------------------------------------
.macro fvm_brg dest:req			# go[>]
    .endm

.macro fvm_brge dest:req		# go[>=]
    .endm
	
.macro fvm_bre dest:req			# go[==]
	sosLeftDown
	dspPrev
	cmpl left, tos
	je \dest
    .endm

.macro fvm_brne dest:req		# go[!=]
	roadwork 69
    .endm
	
.macro fvm_brle dest:req		# go[<=]
    .endm

.macro fvm_brl dest:req			# go[<]
	roadwork 70
    .endm
# -----------------------------------------------------------------------------
.macro fvm_jgz dest:req			# go>0
    .endm

.macro fvm_jgez dest:req		# go>=0
    .endm
	
.macro fvm_jez dest:req			# go==0
    .endm

.macro fvm_jnez dest:req		# go!=0
	roadwork 71
    .endm
	
.macro fvm_jlez dest:req		# go<=0
    .endm

.macro fvm_jlz dest:req			# go<0
	roadwork 72
    .endm
# -----------------------------------------------------------------------------
.macro fvm_jg dest:req			# go>
    .endm

.macro fvm_jge dest:req			# go>=
    .endm
	
.macro fvm_je dest:req			# go==
    .endm

.macro fvm_jne dest:req			# go!=
	roadwork 73
    .endm
	
.macro fvm_jle dest:req			# go<=
    .endm

.macro fvm_jl dest:req			# go<
	roadwork 74
    .endm
# -----------------------------------------------------------------------------
	


	

	
	
# -----------------------------------------------------------------------------
.macro fvm_ret
	ret
    .endm

.macro fvm_invoke
	movl tos, left
	sosDown
	dspPrev
	call *%ebx
    .endm

.macro fvm_rpush
	push tos
	sosDown
	dspPrev
    .endm
	
.macro fvm_rpop
	dspNext
	tosUp
	pop tos
    .endm
	
  .macro fvm_dup
	dspNext
	tosUp
  .endm

  .macro fvm_drop
	sosDown
	dspPrev
  .endm

  .macro fvm_swap
	sosLeftDown
	tosUp
	movl left, tos
  .endm

# -----------------------------------------------------------------------------
.macro sspNext
	subl $dw, ssp
    .endm
.macro sspPrev
	addl $dw, ssp
    .endm
# --------------------------------------
.macro fvm_hold
	sspNext
	movl tos, (ssp)
    .endm
	
.macro fvm_speek
	dspNext
	tosUp
	movl (ssp), tos
    .endm
	
.macro fvm_spush
	sspNext
	movl tos, (ssp)
	sosDown
	dspPrev
    .endm

.macro fvm_spop
	dspNext
	tosUp
	movl (ssp), tos
	sspPrev
	
    .endm

# -----------------------------------------------------------------------------
.macro fvm_dec
	decl tos
    .endm
	
.macro fvm_inc
	incl tos
    .endm
	
.macro fvm_decw
	subl $4, tos
    .endm
	
.macro fvm_incw
	addl $4, tos
    .endm

.macro fvm_dec2w
	subl $8, tos
    .endm
	
.macro fvm_inc2w
	addl $8, tos
    .endm
# -----------------------------------------------------------------------------
.macro fvm_load				# @
	movl (tos), tos
    .endm
	
.macro fvm_store			# !
	roadwork 66
    .endm
	
.macro fvm_load__			# [@]	a1 -- a1 n1
	movl tos, left
	movl (tos), tos
	dspNext
	leftUp
    .endm
# -----------------------------------------------------------------------------
.macro fvm_add
	addl (dsp), tos
	dspPrev
   .endm

.macro fvm_sub
	sosLeftDown
	subl tos, left
	movl left, tos
	dspPrev
   .endm

.macro fvm_mul
	imull (dsp), tos
	dspPrev
   .endm

.macro fvm_div
	movl tos, left
	sosDown
	cdq				# %edx:eax needed or (neg) division wrond
	idivl left
	dspPrev
   .endm

.macro fvm_divmod
	movl tos, left
	sosDown
	cdq				# %edx:eax needed or (neg) division wrond
	idivl left
	tosUp
	movl %edx, tos
   .endm


.macro fvm_cprint
	call print_eax
	sosDown
	dspPrev
   .endm

# -----------------------------------------------------------------------------
.macro fvm_halt
	jmp in_halt
   .endm
	
.macro fvm_data
	jmp in_data
   .endm
	
# -----------------------------------------------------------------------------

# =============================================================================
#   :section:   test
# -----------------------------------------------------------------------------
# =============================================================================
#   :section:   to append
# |===>=<===============================================================>=<===|
